var deepAssign = require('deep-assign');
var commonConfig = require('./../_common');
var prodConfig = require('./_prod');
var customConfig = require('./_custom');

var config = deepAssign(commonConfig, prodConfig, customConfig);
exports.config = config;
