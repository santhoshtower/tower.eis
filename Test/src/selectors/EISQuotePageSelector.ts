
const eisQuotePageSelector = {

    btnAddNewQuote: '//*[@id="quoteForm:newQuoteButton"]',
    dpdwnLOB: '//*[@id="quoteForm:quoteCreationPopupMultiEdit_blob"]',
    dpdwnProduct: '//*[@id="quoteForm:quoteCreationPopupMultiEdit_productCd"]',
    btnNext: '//*[@id="quoteForm:createQuoteButton"]',
}

export default eisQuotePageSelector;