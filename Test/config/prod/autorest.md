
# Scenario: Generate TypeScript interfaces from APIS

> see https://aka.ms/autorest

## Input

```yaml
use-extension:
  "@microsoft.azure/autorest.typescript": "2.0.442"
 
base-folder: ../../APIService
batch:
- typescript: true
  title: VehicleLookup
  input-file: https://api-tac.towercloud.co.nz/vehicle/v1/swagger.json
  output-folder: Vehicle

- typescript: true
  title: AddressLookup
  input-file: https://api-tac.towercloud.co.nz/address/v1/swagger.json
  output-folder: Address

- typescript: true
  title: vehicleUse
  input-file: https://api-tac.towercloud.co.nz/lookup/v1/swagger.json
  output-folder: vehicleUse

sync-methods: true
client-side-validation: false
```