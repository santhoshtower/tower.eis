
# Scenario: Generate TypeScript interfaces from APIS

> see https://aka.ms/autorest

## Input

```yaml
use-extension:
  "@microsoft.azure/autorest.typescript": "2.0.442"
 
base-folder: ../../APIService
batch:
# - typescript: true
#   title: CustomerCore
#   input-file: C:\Users\tristan.worley\Desktop\createCustomerSchema.json
#   output-folder: CustomerCore

- typescript: true
  title: QuoteToBuy
  input-file: C:\Users\tristan.worley\Desktop\createQuote.json
  output-folder: QuoteToBuy


sync-methods: true
client-side-validation: false
```