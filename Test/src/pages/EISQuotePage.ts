import BasePage from './BasePage';

export default class EISLoginPage extends BasePage {
    get btnAddNewQuote(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="quoteForm:newQuoteButton"]') };
    get dpdwnLOB(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="quoteForm:quoteCreationPopupMultiEdit_blob"]') };
    get dpdwnProduct(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="quoteForm:quoteCreationPopupMultiEdit_productCd"]') };
    get btnNext(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="quoteForm:createQuoteButton"]') };

    public addNewQuote(Product) {
        this.btnAddNewQuote.click();
        this.dpdwnLOB.waitForExist();
        browser.waitForSpinner();
        this.dpdwnLOB.selectByVisibleText("Personal Lines");
        browser.waitForSpinner();
        if (Product == "Motor") {
            this.dpdwnProduct.selectByVisibleText("Auto (Preconfigured)");
        }
        else (Product == "HnC")
        {
            this.dpdwnProduct.selectByVisibleText("Home (Preconfigured)");
        }
        browser.waitForSpinner();
        this.btnNext.click();
        browser.waitForSpinner();
    }
}
