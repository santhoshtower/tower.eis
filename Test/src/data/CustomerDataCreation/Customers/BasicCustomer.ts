const BasicCustomer = {
    createCustomer: {
        customerNumber: "K59400438246",
        customerType: "INDIVIDUAL_LEAD",
        customerStatus: "QUALIFIED",
        sourceCd: "INTERNET",
        ratingCd: "RATING",
        individualDetails: {
            firstName: "customerFirstName9999",
            lastName: "customerLastName9999",
        }
    },
    quotes: [{
        customerNumber: "510026",
        effectiveDate: "2018-10-25T09:42:10Z",
        inceptionDate: "2018-09-12T08:00:00Z",
        expirationDate: "2018-11-01T01:48:44Z",
        sourceCd: "NEW",
        declarationStatusCd: "ANSW",
        typeOfPolicyCd: "PMT",
        rateEffectiveDate: "2018-09-11T08:00:00Z",
        rateEffectiveDateOverridenInd: false,
        producerCd: "Tower",
        subProducerCd: "Tower",
        brandCd: "Tower",
        policyCount: 1,
        campaignCode: "CAMPAIGN_CODE",
        externalCustomerNo: "1223",
        insuredPrivacyDeclarationAgreedInd: true,
        insuredCustomerDisclosureDeclarationAgreedInd: true,
        insured48HourStandDownInformedInd: true,
        paymentPlanCd: "ANNUAL",
        packageCd: "Comp_Agreed",
        insuredsAndDrivers: [
            {
                individual: {
                    firstName: "John",
                    lastName: "Smith",
                    dateOfBirth: "1984-09-28",
                    genderCd: "male"
                },
                phones: [
                    {
                        phone: "006412345678",
                        phoneCountryCd: "NZ"
                    }
                ],
                emails: [
                    {
                        email: "john.smith@email.com"
                    }
                ],
                insured: {
                    primaryInsuredInd: true,
                    claimsDeclinedInd: false,
                    convictionsInd: true,
                    convictions: [
                        {
                            convictionTypeCd: "BURGL",
                            convictionYear: 2004
                        }
                    ],
                    carInsuranceDeclinedInd: true,
                    bankruptedInd: true
                },
                driver: {
                    driverTypeCd: "MD",
                    lossDamageInd: true,
                    lossDamages: [
                        {
                            lossDamageTypeCd: "COLEP",
                            lossDamageYear: 2017,
                            driverLossExcessPaidInd: true
                        }
                    ],
                    offenceInd: true,
                    offences: [
                        {
                            offenceTypeCd: "CLSDRV",
                            offenceYear: 2016
                        }
                    ],
                    licenses: [
                        {
                            howLongHeldYourDriverLicenseCd: "001",
                            licenseWasSuspendedInd: false
                        }
                    ]
                }
            }
        ],
        vehicles: [
            {
                marketValue: 1000,
                agreedValueMin: 1000,
                agreedValueMax: 3000,
                adjustedMarketValue: 2000,
                costNew: 5000,
                vehicleUsageCd: "TRDPRS",
                registrationNo: "123456",
                make: "AUDI",
                model: "100",
                year: "1993",
                bodyStyle: "Sedan",
                type: "E Sedan 4dr Auto 4sp 2.6i",
                otherModelDescription: "other model description",
                alarmInd: true,
                useStatusCd: "5N7",
                registeredAndWarrantedInd: true,
                declaredAnnualKilometresCd: "15000",
                useForBusinessInd: false,
                ownVehicleInd: true,
                parkingLocationCd: "GARAGE",
                parkingSameAsInsuredAddressInd: true,
                parkingAddress: {
                    city: "Wellington",
                    stateProvCd: "WLG",
                    postalCode: "6011",
                    countryCd: "NZ",
                    addressLine1: "Courtney Place",
                    addressLine2: "11",
                    addressLine3: "2N",
                    referenceId: "0",
                    addressValidatedInd: true
                },
                hotListFlags: {
                    declineInd: false,
                    referInd: false,
                    driverU21Ind: false,
                    driverU25Ind: false,
                    vehicleExcessInd: false,
                    theftExcessInd: false,
                    alarmInd: true,
                    windscreenInd: false
                },
                excessAmount: 1000,
                coverages: {
                    vehicleScheduledItemsCoverage: {
                        limitAmount: 2000,
                        limitAmountDisplay: "2000"
                    },
                    vehicleRoadWiseBenefitCoverage: {
                        limitAmount: 1000
                    },
                    vehicleTrailersCoverage: {
                        limitAmount: 3000
                    },
                    vehicleU25DriverExclusionBenefitCoverage: {
                        limitAmount: 4000
                    },
                    vehicleWindscreenAndWindowGlassExcessBuyOutCoverage: {
                        limitAmount: 5000
                    }
                }
            }
        ]
    }],
    policies: [{

    }],
    claims: [{

    }
    ],
}

export { BasicCustomer };