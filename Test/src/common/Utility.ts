class Utility {

    static getKeyByFieldValue(data, value, field = 'id'): string {
        for (let key in data) {
            if (value === data[key][field]) {
                return key;
            }
        }
        return null;
    }
}

export { Utility }