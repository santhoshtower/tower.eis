import { expect } from 'chai';
import { Given, When, Then } from 'cucumber';
import EISLoginPage from '../../pages/EISLoginPage';
import EISHomePage from '../../pages/EISHomePage';
import eisQuotePage from '../../pages/EISQuotePage';
import MotorPage from '../../pages/EISMotorPage';
import { CustomerData } from '../../common/CustomerData';
import eisHNCPage from '../../pages/EISHouseAndContentsPage';
import { SharedData } from '../../common/CommonData';
//import { EISLogin } from '../../steps/UI/EISLogin';
//import {moment} from 'moment';

var environment = CustomerData.Environment.Tac;
const page = new EISLoginPage(environment);
const homePage = new EISHomePage(environment);
const quotePage = new eisQuotePage(environment);
const motorPage = new MotorPage(environment);
//const loginPage = new EISLogin(environment);
const HNCPage = new eisHNCPage(environment);

Given(/^I create the "([^"]*)?" Policy for the Customer$/, async (Product) => {
    homePage.selectAddQuote();
    quotePage.addNewQuote(Product);
});

When(/^I enter a "([^"]*)?" , "([^"]*)?" Policy information and Policy Count "([^"]*)?"$/, async (Product, PolicyType, PolicyCount) => {
    if (Product == "Motor") {
        var Policy: string = PolicyType;
        motorPage.selectPolicyType(PolicyType);

        // expect(motorPage.dpdwnSource.getValue()).to.be.equal(MotorQuoteData.MotorQuoteDataValidation.source);
        // expect(motorPage.dpwnDeclarationStatus.getValue()).to.be.equal(MotorQuoteData.MotorQuoteDataValidation.DeclrationStatus);

        // expect(motorPage.dpdwnSource.getValue()).to.be.equal(MotorQuoteData.MotorQuoteDataValidation.source);
        // expect(motorPage.dpwnDeclarationStatus.getValue()).to.be.equal(MotorQuoteData.MotorQuoteDataValidation.DeclrationStatus);
    }
    else if (Product == "HnC") {
        var Policy: string = PolicyType;
        switch (Policy) {
            case "House": {
                console.log(PolicyType);
                HNCPage.dpdwnSource.selectByVisibleText(PolicyType);
                break;
            }
            case "Contents": {
                console.log(PolicyType);
                HNCPage.dpdwnSource.selectByVisibleText(PolicyType);
                break;
            }
            case "Landlords House": {
                console.log(PolicyType);
                HNCPage.dpdwnSource.selectByVisibleText(PolicyType);
                break;
            }
        }
    }
    browser.waitForSpinner();
    motorPage.setPolicyCount(PolicyCount);
});

When(/^I enter the "([^"]*)?" Insured information$/, async (Product) => {
    motorPage.acceptDeclarations(true, true, true);

    if (Product == "Motor") {
        motorPage.selectInsuredParty(SharedData.fullname);
    } else if (Product == "HnC") {
        HNCPage.dpdwnInsuredPartySelection.selectByVisibleText(SharedData.fullname);
        browser.waitForSpinner();
    }

    // motorPage.txtboxDOBInsured.setValue("01/01/1990");
    // browser.keys("Enter");
    // browser.waitForSpinner();
    motorPage.selectClaimsDeclined(false);

    if (Product == "Motor") {
        motorPage.selectInsuranceDeclined();
        motorPage.selectCriminalConvictions();
        motorPage.selectBankruptcy();
    } else if (Product == "HnC") {
        HNCPage.rdbtnInsuranceRefusedCancelled.click();
        browser.waitForSpinner();
        HNCPage.rdbtnLossDamaged.click();
        browser.waitForSpinner();
        HNCPage.rdbtnConvictions.click();
        browser.waitForSpinner();
        HNCPage.rdbtnBankRuptcy.scroll(0, 1000);
        browser.waitForSpinner();
        HNCPage.rdbtnBankRuptcy.click();
    }

    browser.waitForSpinner();
    motorPage.selectNext();
});
// HnC Tab Steps
When(/^I enter details in Risk Address Tab$/, (): void => {
    motorPage.selectNext();
    HNCPage.InsureAddress.click();
    browser.waitForSpinner();
    HNCPage.SearchAddressInsured.setValue(CustomerData.ValidInd.SearchAddress);
    browser.pause(500);
    browser.waitForSpinner();
    browser.pause(1500);
    HNCPage.SelectAddress.click();

    browser.waitForSpinner();
    HNCPage.dpdwnDwellingType.selectByVisibleText(CustomerData.HouseDetails.DwellingDetails);
    browser.waitForSpinner();
    HNCPage.rdbtnBodyCorporate_No.click();
    browser.waitForSpinner();
    HNCPage.dpdwnStoreis.selectByVisibleText("1");
    browser.waitForSpinner();
    HNCPage.dpdwnUnits.selectByVisibleText("1");
    browser.waitForSpinner();
    console.log("Units");
    HNCPage.txtBxYearBuilt.setValue(CustomerData.HouseDetails.YearBuilt);
    browser.keys("Enter");
    browser.waitForSpinner();
    console.log("YearBuilt");
    HNCPage.dpdwnRoofType.selectByVisibleText(CustomerData.HouseDetails.RoofType);
    browser.waitForSpinner();
    HNCPage.dpdwnConstructionType.selectByVisibleText(CustomerData.HouseDetails.ConstType);
    browser.waitForSpinner();
    HNCPage.txtBxFloorArea.setValue(CustomerData.HouseDetails.FloorArea);
    browser.keys("Enter");
    browser.waitForSpinner();
    HNCPage.rdbtnExternalGarage.click();
    browser.waitForSpinner();
    HNCPage.txtbxSumInsured.setValue(CustomerData.HouseDetails.SumInsured);
    browser.keys("Enter");
    browser.waitForSpinner();
    HNCPage.rdbtnWaterTight.click();
    browser.waitForSpinner();
    HNCPage.rdbtnAirBnB.scroll(0, 1000);
    HNCPage.rdbtnHazard.click();
    browser.waitForSpinner();
    HNCPage.dpdwnOccupancyType.selectByVisibleText(CustomerData.HouseDetails.OccupancyType);
    browser.waitForSpinner();
    HNCPage.rdbtnAirBnB.click();
    browser.waitForSpinner();
    motorPage.selectSave();
});

When(/^I do Junk stuff in HNC$/, (): void => {
    motorPage.selectNext();
    HNCPage.InsureAddress.click();
    browser.waitForSpinner();
    HNCPage.SearchAddressInsured.setValue(CustomerData.ValidInd.SearchAddress);
    browser.waitForSpinner();
    browser.pause(1000);
    HNCPage.SelectAddress.click();
    // browser.waitForSpinner();
    // HNCPage.dpdwnState.selectByVisibleText("ALBERT STREET");
    // browser.waitForSpinner();
    // HNCPage.ValidateBtn.click();
    browser.waitForSpinner();
});

When(/^I enter details in CoveragesandPremium$/, (): void => {
    HNCPage.tabCnP.click();
    browser.waitForSpinner();
    HNCPage.dpdwnPaymentPlan.selectByIndex(1);
    browser.waitForSpinner();
    HNCPage.dpdwnPolicyExcess.selectByIndex(1);
    browser.waitForSpinner();
    HNCPage.txtBxDwellingLimit.setValue("1000");
    browser.keys("Enter");
    browser.waitForSpinner();
    HNCPage.txtBxPersonalProperty.setValue("1000");
    browser.keys("Enter");
    browser.waitForSpinner();
    // HNCPage.txtBxLossofUse.setValue("1000");
    // browser.keys("Enter");
    // browser.waitForSpinner();
    // HNCPage.txtBxFairRentalValue.setValue("1000");
    // browser.keys("Enter");
    // browser.waitForSpinner();
    // HNCPage.txtBxAdditionalLivingExp.setValue("1000");
    // browser.keys("Enter");
    // browser.waitForSpinner();
    HNCPage.btnCalculatePremium.scroll(0, 1000);
    HNCPage.btnCalculatePremium.click();
    browser.waitForSpinner();
    HNCPage.tabDocument.click();
    browser.waitForSpinner();
    motorPage.selectSave();
    HNCPage.btnSaveNext.click();
    browser.waitForSpinner();
    browser.pause(1000);
});

When(/^I issue the Policy$/, (): void => {
    HNCPage.dpdwnTakeAction.selectByVisibleText("Issue");
    browser.waitForSpinner();
    motorPage.finishQuote();

    // motorPage.btnFinish.click();
    // browser.waitForSpinner();
});

When(/^I enter the "([^"]*)?" Insured information and 48 standdown to No$/, async (Product) => {
    motorPage.acceptDeclarations(true, true, false);
    // motorPage.rdBtnPrivacyDeclaration.click();
    // browser.waitForSpinner();
    // motorPage.rdBtnDisclosureDeclaration.click();
    // browser.waitForSpinner();
    // motorPage.rdbtn48HrStandDown_NO.click();
    // browser.waitForSpinner();
});

When(/^I update the declaration details for (.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?)$/, async (Product, ClaimsDeclined, CDReason, InsuranceRefused, IRReason, Loss, LossType, LossYear, Convictions, ConvictionType, CYear, Bankruptcy, BType, BYear: string) => {
    console.log(Product);

    if (Product == "Motor") {
        motorPage.selectInsuredParty(SharedData.fullname);
    } else if (Product == "HnC") {
        HNCPage.dpdwnInsuredPartySelection.selectByVisibleText(SharedData.fullname);
    }
    console.log(Product, ClaimsDeclined, CDReason, InsuranceRefused, IRReason, Loss, LossType, LossYear, Convictions, ConvictionType, CYear, Bankruptcy, BType, BYear);

    browser.waitForSpinner();

    if (ClaimsDeclined == "Yes") {
        motorPage.selectClaimsDeclined(true);
        motorPage.selectClaimsDeclinedReason(CDReason);

    } else {
        motorPage.selectClaimsDeclined(false);
    }
    browser.waitForSpinner();
    if (Product == "Motor") {
        motorPage.selectInsuranceDeclined();
        motorPage.selectCriminalConvictions();
        motorPage.selectBankruptcy();
    } else if (Product == "HnC") {
        if (InsuranceRefused == "Yes") {
            HNCPage.rdbtnInsuranceRefusedCancelled_Yes.click();
            browser.waitForSpinner();
            HNCPage.dpdwnInsuranceRefused.selectByVisibleText(IRReason);
            browser.waitForSpinner();
        } else { HNCPage.rdbtnInsuranceRefusedCancelled.click(); }
        browser.waitForSpinner();
        if (Loss == "Yes") {
            HNCPage.rdbtnLossDamagedYes.click();
            browser.waitForSpinner();
            HNCPage.dpdwnLossType.selectByVisibleText(LossType);
            browser.waitForSpinner();
            HNCPage.txtBoxLossYear.scroll(0, 500);
            HNCPage.txtBoxLossYear.setValue(LossYear);
            browser.waitForSpinner();
        } else {
            HNCPage.rdbtnLossDamaged.click();
        }
        browser.waitForSpinner();
        if (Convictions == "Yes") {
            HNCPage.rdbtnConvictions.scroll(0, 1000);
            browser.waitForSpinner();
            browser.pause(500);
            HNCPage.rdbtnConvictions_Yes.click();
            browser.waitForSpinner();
            HNCPage.rdbtnConvictions_Yes.click();
            browser.waitForSpinner();
            HNCPage.dpdwnConvictionType.selectByVisibleText(ConvictionType);
            browser.waitForSpinner();
            HNCPage.txtBoxConvictionYear.setValue(CYear);
            browser.waitForSpinner();
        } else {
            HNCPage.rdbtnConvictions.click();
        }
        browser.waitForSpinner();
        HNCPage.rdbtnBankRuptcy.scroll(0, 1000);
        if (Bankruptcy == "Yes") {
            HNCPage.rdbtnBankRuptcy_Yes.click();
            browser.waitForSpinner();
            HNCPage.rdbtnBankRuptcy_Yes.click();
            browser.waitForSpinner();
            HNCPage.dpdwnBankruptcy.selectByVisibleText(BType);
            browser.waitForSpinner();
            HNCPage.txtBoxBankruptcyYear.setValue(BYear);
            browser.waitForSpinner();
        } else {
            HNCPage.rdbtnBankRuptcy.click();
            browser.waitForSpinner();
        }

        browser.waitForSpinner();
    }

    browser.waitForSpinner();
    motorPage.selectNext();
    browser.waitForSpinner();

});