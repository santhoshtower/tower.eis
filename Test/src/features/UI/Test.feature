@ui
Feature: Santhosh Learning TS

    Feature Description: Santhosh Learning TS

    # Scenario: Learning
    #     When I do Blah blah test
    #         | PolicyType     | Type              | FName      | LName      | DOB        | Gender | LicenseAge       | Suspension | Offence | OType            | OYear | Damage | DamageType | DYear |
    #         | Personal Motor | Occasional Driver | TestFName1 | TestLName1 | 01/01/1990 | Male   | Less than 1 year | No         | Yes     | Careless driving | 2018  | Yes    | Theft      | 2017  |
    Background: Simple background
        Given I Login into the EIS SIT

    Scenario: Create an Individual Customer without Relationship
        # Given I Login into the EIS SIT
        Given I create a Customer without Relationship
        Then I validate the Customer Details "without" Relationship

    Scenario: Create an Individual Customer without Relationship 1
        # Given I Login into the EIS SIT
        Given I create a Customer without Relationship
        Then I validate the Customer Details "without" Relationship


# Background: Customer Search
#     Given I Login into the EIS SIT
#     And I search for "Customer" with string "Santhosh Ippili Tester"

# Scenario: Create a New Personal Motor Policy for a Customer

#     Given I create the "Motor" Policy for the Customer
#     When I enter a "Motor" , "Personal Motor" Policy information and Policy Count "10"
#     And I enter the "Motor" Insured information
#     And I enter the "Personal Motor" Details
#     And I enter the Driver information for "Personal Motor"
#     And I add anotherDriver with Details
#         | PolicyType     | Type              | FName      | LName      | DOB        | Gender | LicenseAge       | Suspension | Offence | OType            | OYear | Damage | DamageType | DYear |
#         | Personal Motor | Occasional Driver | TestFName1 | TestLName1 | 01/01/1990 | Male   | Less than 1 year | No         | Yes     | Careless driving | 2018  | Yes    | Theft      | 2017  |
#         | Personal Motor | Occasional Driver | TestFName2 | TestLName2 | 01/01/1990 | Male   | Less than 1 year | No         | Yes     | Careless driving | 2018  | Yes    | Theft      | 2017  |
#     And I enter the FormsAddl info
#     And I choose the plan "Comprehensive Agreed" Premium&Coverages for "Personal Motor"



# Scenario Outline: Create a New House Policy for a New Customer
#     Given I create the "<Policy>" Policy for the Customer
#     When I enter a "<Policy>" , "House" Policy information and Policy Count "10"
#     And I enter the "<Policy>" Insured information and 48 standdown to No
#     And I update the declaration details for <Policy>,<ClaimsDeclined>,<CDReason>,<InsuranceRefused>,<IRReason>,<Loss>,<LossType>,<LossYear>,<Convictions>,<ConvictionType>,<CYear>,<Bankruptcy>,<BType>,<BYear>
#     And I enter details in Risk Address Tab
#     And I enter details in CoveragesandPremium
#     And I issue the Policy

#     Examples:
#         | Policy | ClaimsDeclined | CDReason      | InsuranceRefused | IRReason                          | Loss | LossType | LossYear | Convictions | ConvictionType | CYear | Bankruptcy | BType      | BYear |
#         | HnC    | Yes            | Other reasons | Yes              | Change of risk profile by insured | Yes  | Fire     | 2017     | Yes         | Arson          | 2016  | Yes        | Bankruptcy | 2017  |