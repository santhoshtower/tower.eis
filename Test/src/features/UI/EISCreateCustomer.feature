@ui
Feature: Create a Individual Customer
    QA Engineer should be logged into EIS - SIT

    # Scenario: Create an Individual Customer with Individual as Relationship
    #     Given I Login into the EIS SIT
    #     And I create a Customer with Individual Relationship
    #     Then I validate the Customer Details "with" Relationship

    # Scenario: Create an Individual Customer without Relationship
    #     Given I Login into the EIS SIT
    #     And I create a Customer without Relationship
    #     Then I validate the Customer Details "without" Relationship

    Scenario: Create an Individual Customer without Relationship and with Airpoints
        Given I Login into the EIS SIT
        When I Create a Customer with Airpoints Sarath, Samarasekara, 45003064
        Then I validate the Customer Details "without" Relationship


