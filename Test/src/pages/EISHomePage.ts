import BasePage from './BasePage';
import EISRelationshipPage from './EISRelationshipPage';

export default class EISHomePage extends BasePage {

    get customerTab(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="tabForm:topTabsBarList:1:linkLabel"]') };
    get createCustomer(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="searchForm:createAccountBtnAlway"]') };
    get rdBtnCreateIndiviudal(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="searchForm:customerType:0"]') };
    get rdBtnCreateNonIndiviudal(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="searchForm:customerType:1"]') };
    get btnYes(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="searchForm:yes"]') };
    get txtbxFirstName(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:generalInfo_firstName"]') };
    get txtbxLastName(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:generalInfo_lastName"]') };
    get txtbxSearchAddress(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:addressTypeInfo_0_addressQueryStr_input"]') };
    get selectAddress(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:addressTypeInfo_0_addressQueryStr_panel"]/table/tbody/tr[1]') };
    get btnNext(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:nextBtn_footer"]') };
    get btnDone(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:doneBtn_footer"]') };
    get lblCustomerName(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="custInfoForm:name"]') };
    get lblAddress(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="custInfoForm:j_id_27_58_1_3:addressLine"]') };
    get txtBoxDOB(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:generalInfo_birthDateInputDate"]') };
    get dpdwnSalutation(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:generalInfo_salutation"]') };
    get dpdwnGender(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:generalInfo_genderCd"]') };
    get btnAddQuote(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="quotes:addPolicyBtn"]') };
    get rdBtnPaperless(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:additionalInfo_paperless"]') };

    // non indi elements:
    get dpdwnNonIndiType(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:generalInfoLeft_businessType"]') };
    get txtBoxNameLegal(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:generalInfoLeft_legalName"]') };
    get txtBoxCompanyNo(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:generalInfoLeft_legalId"]') };

    //Airpoints into action.
    get btnAddGroup(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:groupInfoMethod:addGroupInfoEvent"]') };
    get btnGroupSearch(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:showGroupSearchPopup_0"]') };
    get btnPopupSearch(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="assignGroupInfoForm:groupSearchBtn"]') };
    get linkAirpointsSelect(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="assignGroupInfoForm:searchResultTable:0:showGroupSearchPopup"]') };
    get txtBxAPFname(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:generalInfo_0_membershipFirstName"]') };
    get txtBxARLName(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:generalInfo_0_membershipLastName"]') };
    get txtBxAirpointsNo(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:generalInfo_0_membershipNumber"]') };

    // Relationship Page Elements
    get rdbtnRelationShipType_Ind(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:relationshipType_0:0"]') };
    get rdbtnRelationShipType_NonInd(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:relationshipType_0:1"]') };

    public addAirpoints(firstName: any, lastName: any, airpointsNumber: string): any {
        this.btnAddGroup.click();
        browser.waitForSpinner();
        this.btnGroupSearch.click();
        browser.waitForSpinner();
        this.btnPopupSearch.click();
        browser.waitForSpinner();
        this.linkAirpointsSelect.click();
        browser.waitForSpinner();
        this.txtBxAPFname.setValue(firstName);
        browser.waitForSpinner();
        this.txtBxARLName.setValue(lastName);
        browser.waitForSpinner();
        this.txtBxAirpointsNo.scroll(0, 1000);
        this.txtBxAirpointsNo.setValue(airpointsNumber);
        browser.waitForSpinner();
    }

    public selectNext(): any {
        this.btnNext.click();
        browser.waitForSpinner();
    }

    public getAddress(): string {
        return this.lblAddress.getText();
    }

    public getCustomerName(): string {
        return this.lblCustomerName.getText();
    }

    public selectAddQuote() {
        this.btnAddQuote.click();
    }

    public setAddress(address) {
        console.log("Setting address now");
        browser.waitForSpinner();
        this.txtbxSearchAddress.waitForVisible();
        browser.waitForSpinner();
        console.log("Setting address value now spinner is gone");
        this.txtbxSearchAddress.setValue(address);
        browser.pause(500); //give it time to load (replace with implicit wait later)
        browser.waitForSpinner();
        console.log("spinner gone after set address");
        this.selectAddress.click();
        browser.pause(500);
        browser.waitForSpinner();
        // browser.pause(1000);
        this.rdBtnPaperless.scroll(0, 1000);
        browser.waitForSpinner();
        this.rdBtnPaperless.click();
        browser.waitForSpinner();
        this.btnNext.click();
        browser.waitForSpinner();
        this.selectDone();
    }

    public selectDone() {
        this.btnDone.click();
    }

    public createCustomerWithIndividualRelationship(customer, firstName, lastName, address) {
        this.customerTab.click();
        this.createCustomer.click();
        //var btnStatus = this.btnYes.isEnabled();
        //console.log(btnStatus);
        // if (this.btnYes.isEnabled() == false) {
        //     expect(btnStatus).to.be.false;
        // }

        //console.log(uniqueId1);
        this.rdBtnCreateIndiviudal.click();
        this.btnYes.click();
        this.dpdwnSalutation.selectByVisibleText(customer.Salutation);
        this.dpdwnGender.selectByVisibleText(customer.Gender);
        this.txtBoxDOB.setValue(customer.DOB);
        this.txtbxFirstName.setValue(firstName);
        this.txtbxLastName.setValue(lastName);
        //console.log(firstName + " " + lastName);
        this.setAddress(address);

        return new EISRelationshipPage(this.url);
    }

    public createCustomerWithRelationship(address, uniqueId) {
        this.customerTab.click();
        browser.waitForSpinner();
        this.createCustomer.click();
        browser.waitForSpinner();
        this.rdBtnCreateNonIndiviudal.click();
        browser.waitForSpinner();
        this.btnYes.click();
        browser.waitForSpinner();
        this.dpdwnNonIndiType.selectByIndex(1);
        browser.waitForSpinner();
        this.txtBoxNameLegal.setValue("NameLegal" + " " + uniqueId);
        browser.waitForSpinner();
        this.txtBoxCompanyNo.setValue("1111111");
        this.setAddress(address);
        this.btnNext.click();
        browser.waitForSpinner();
    }


    public createCustomerWithoutRelationship(salutation, firstName, lastName, gender, dob, address, airpointsNumber): any {
        this.customerTab.click();
        this.createCustomer.click();
        this.rdBtnCreateIndiviudal.click();
        this.btnYes.click();
        this.dpdwnSalutation.selectByVisibleText(salutation);
        this.txtbxFirstName.setValue(firstName);
        this.txtbxLastName.setValue(lastName);
        this.dpdwnGender.selectByVisibleText(gender);
        this.txtBoxDOB.setValue(dob);
        if (airpointsNumber !== null) {
            this.addAirpoints(firstName, lastName, airpointsNumber);
        }
        this.setAddress(address);
    }
}
