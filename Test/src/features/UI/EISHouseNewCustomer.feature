@ui
Feature: Create a New House and Contents Policy for a New Customer
    QA Engineer should be logged into EIS - SIT
    Creates a new Customer with a Relationship and creates a House Policy

    Background: Customer Search and opening the Customer
        Given I Login into the EIS SIT
        And I create a Customer without Relationship
        Then I validate the Customer Details "without" Relationship


    Scenario: Create a New House Policy for a New Customer
        Given I create the "HnC" Policy for the Customer
        When I enter a "HnC" , "House" Policy information and Policy Count "1"
        And I enter the "HnC" Insured information
        # And I do Junk stuff in HNC
        And I enter details in Risk Address Tab
        And I enter details in CoveragesandPremium
        And I issue the Policy

# Scenario: Create a New Contents Policy for a New Customer
#     Given I create the "HnC" Policy for the Customer
#     When I enter a "HnC" , "Contents" Policy information
#     And I enter the "HnC" Insured information
#     And I do Junk stuff in HNC
#     And I enter details in Risk Address Tab
#     And I enter details in CoveragesandPremium
#     And I issue the Policy

# Scenario: Create a New Landlords House Policy for a New Customer
#     Given I create the "HnC" Policy for the Customer
#     When I enter a "HnC" , "Landlords House" Policy information
#     And I enter the "HnC" Insured information
#     And I do Junk stuff in HNC
#     And I enter details in Risk Address Tab
#     And I enter details in CoveragesandPremium
#     And I issue the Policy
