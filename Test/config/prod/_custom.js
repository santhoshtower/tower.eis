const domian = "https://twr-tac.towercloud.co.nz";
module.exports = {
    CAR_QUOTE_PAGE: `${domian}`,
    CAR_QUOTE_PAGE_ONE: `${domian}/car/quote/page1`,
    CAR_QUOTE_PAGE_TWO: `${domian}/car/quote/page2`,
    CAR_QUOTE_PAGE_THREE: `${domian}/car/quote/page3`,
    API: `https://api-tac.towercloud.co.nz/`,
};