import { SharedData } from '../../common/CommonData';
import { After, Before, setDefaultTimeout } from 'cucumber';

Before(async () => {
    setDefaultTimeout(30 * 1000);
    SharedData.generateUniqueId();
});

After(async () => {
    SharedData.reset();
});