const parameters = {
    API: `https://api-tac.towercloud.co.nz/`,
};

module.exports = {
    featurePath: './src/features/API/*.feature',
    requiredFiles: '--require ./src/steps/API/*.ts',
    tags: '--tags "@api" --tags "not @prod"',
    worldParameters: `--world-parameters '{ "api":"${parameters.API}"}'`
}