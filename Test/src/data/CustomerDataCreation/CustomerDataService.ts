import { QuoteToBuy, } from '../../../APIService/QuoteToBuy/QuoteToBuy';
import { PrecAuQuoteCreateRequest } from '../../../APIService/QuoteToBuy/models/index';

class CustomerDataService {
    private static createCustomerResult;
    private static customersService = "http://eis-ts01.towercloud.co.nz:8080/twr-app/services/customercore-rs/v1/customers";
    private static quoteServiceUrl = "https://dev5-twr-app20-gateway.eisgroup.com";

    public static async createCustomer(customerDetail) {

        const customerEntry = await this.createCustomerEntry(customerDetail.createCustomer);
        console.log("customerEntry result: ", customerEntry);

        for (var i = 0; i < customerDetail.quotes.length; i++) {
            // add quote ids to customerEntry?
            const quoteResult = await this.createCustomerQuotes(customerEntry.customerNumber, customerDetail.quotes[i]);
            console.log("quoteResult result: ", quoteResult);
        }
        // for (var i = 0; i < customerDetail.policies.length; i++) {
        //     this.createCustomerPolicies(customerEntry.customerNumber, customerDetail.quotes[i]);
        // }
        // for (var i = 0; i < customerDetail.claims.length; i++) {
        //     this.createCustomerClaims(customerEntry.customerNumber, customerDetail.quotes[i]);
        // }
        return CustomerDataService.createCustomerResult;
    }

    public static async createCustomerQuotes(customerId, requestBody) {
        requestBody.customerid = customerId
        var quoteService = new QuoteToBuy(this.quoteServiceUrl);
        const params = <PrecAuQuoteCreateRequest>requestBody;
        return await quoteService.createQuoteWithHttpOperationResponse(params);
    }

    public static async createCustomerEntry(requestBody) {
        console.log("Sending request to server: ", this.customersService);
        var request = require('request-promise');
        let options = {
            url: this.customersService,
            method: "POST",
            headers: {
                "content-type": "application/json",
            },
            auth: {
                'user': 'qa',
                'pass': 'qa',
            },
            body: requestBody,

            json: true,
        };

        await request(options).then(function (result) {
            CustomerDataService.createCustomerResult = result;
        }).catch(function (err) {
            console.log("create customer error ", err);
            CustomerDataService.createCustomerResult = null;
        });
        return CustomerDataService.createCustomerResult;
    }
}
export { CustomerDataService }