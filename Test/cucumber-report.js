var reporter = require('multiple-cucumber-html-reporter');

reporter.generate({
    jsonDir: './tmp/',
    reportPath: './tmp/report',
    pageTitle: "EIS Portal Tests",
    reportName: "EIS Portal Tests",
    displayDuration: true
});