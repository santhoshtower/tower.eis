import { Given, When, Then } from 'cucumber';
import { CustomerData } from '../../common/CustomerData';
import EISSearchPlusPage from '../../pages/EISSearchPlusPage';
import { SharedData } from '../../common/CommonData';

var environment = CustomerData.Environment.Tac;
const searchPlusPage = new EISSearchPlusPage(environment);

Given(/^I search for "([^"]*)?" with string "([^"]*)?"$/, async (tabtoclick, stringtoSearch) => {
    searchPlusPage.performSearch(tabtoclick, stringtoSearch);
});

