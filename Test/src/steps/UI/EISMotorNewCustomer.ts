import { expect } from 'chai';
import { Given, When, Then } from 'cucumber';
import EISLoginPage from '../../pages/EISLoginPage';
import EISHomePage from '../../pages/EISHomePage';
import eisQuotePage from '../../pages/EISQuotePage';
import MotorPage from '../../pages/EISMotorPage';
import { CustomerData } from '../../common/CustomerData';
import { SharedData } from '../../common/CommonData';
//import { EISLogin } from '../../steps/UI/EISLogin';

var environment = CustomerData.Environment.Tac;
const page = new EISLoginPage(environment);
const homePage = new EISHomePage(environment);
const quotePage = new eisQuotePage(environment);
const motorPage = new MotorPage(environment);
//const loginPage: new EISLogin("http://azs4siteisapp01:8080/twr-app/login.xhtml#noback");


When('I enter the Policy information', (): void => {
    motorPage.enterPolicyInformation(SharedData.fullname, "01/01/1990");
});

When(/^I enter the Driver information for "([^"]*)?"$/, async (PolicyType) => {
    if (PolicyType == "Personal Motor" || PolicyType == "Commercial Motor" || PolicyType == "Motorbike" || PolicyType == "Motorhome") {
        motorPage.enterDriverInfo(SharedData.fullname, CustomerData.ValidInd.DriverGender, CustomerData.ValidInd.DriverType, CustomerData.ValidInd.DriverLicenseAge);
    }
    else if (PolicyType == "Caravan" || PolicyType == "Trailer (stand alone)") {
        console.log("skip Caravan driver info");
        console.log(PolicyType);
        motorPage.selectNext();
    }
});

When(/^I enter the "([^"]*)?" Details$/, async (PolicyType) => {
    console.log("Enter personal motor details");
    switch (PolicyType) {
        case "Personal Motor": {
            motorPage.addPersonalVehicle(CustomerData.VehicleDetails, CustomerData.ValidInd.SearchAddress);
            console.log("vehicle added");
            break;
        }
        case "Commercial Motor": {
            console.log(PolicyType);
            browser.waitForSpinner();
            motorPage.addCommercialVehicle(CustomerData.VehicleDetails, CustomerData.ValidInd.SearchAddress, "Tradesperson");

            break;
        }
        case "Motorbike": {
            console.log(PolicyType);
            motorPage.addMotorBike(CustomerData.MotorBikeDetails, CustomerData.ValidInd.SearchAddress, false);
            break;
        }
        case "Caravan": {
            console.log(PolicyType);
            motorPage.addCaravan(CustomerData.CaravanDetails, CustomerData.ValidInd.SearchAddress);
            break;
        }
        case "Motorhome": {
            console.log(PolicyType);
            motorPage.addCaravan(CustomerData.CaravanDetails, CustomerData.ValidInd.SearchAddress, CustomerData.VehicleDetails.Distance);
            break;
        }
        case "Trailer (stand alone)": {
            console.log(PolicyType);
            motorPage.addTrailer(CustomerData.TrailerDetails, CustomerData.ValidInd.SearchAddress);
            break;
        }
    }
});

When('I enter the MVRClaims', (): void => {
    motorPage.selectNext();
});

When('I enter the FormsAddl info', (): void => {
    motorPage.selectNext();
    motorPage.selectSave();
    motorPage.selectNext();
});

When(/^I choose the plan "([^"]*)?" Premium&Coverages for "([^"]*)?"$/, async (Plan, PolicyType) => {
    motorPage.selectPlan(Plan);
    console.log("Plan selected");
    motorPage.selectExcessByIndex(1);
    console.log("excess selected");
    if (PolicyType === "Commercial Motor") {
        motorPage.setCoverages("$1,000.00");
    }
    motorPage.calculatePremium();
});

When(/^I Finish Quote in Document Request Tab/, (): void => {
    motorPage.finishQuote();
});

When('I do junk stuff to bypass', (): void => {
    motorPage.junkStuff_Temp(CustomerData.ValidInd.SearchAddress);
});

When('I do more junk stuff to bypass', (): void => {
    motorPage.moreJunkStuff_Temp();
});

When('I do more junk stuff to bypass and help Tania', (): void => {
    motorPage.evenMoreJunkStuff_Temp();
});

When(/^I add anotherDriver with Details$/, async (Table) => {
    const dataTable = Table.hashes();
    for (let i: 0; i < dataTable.length; i++) {
        var driver = {
            policyType: dataTable[i].PolicyType,
            type: dataTable[i].Type,
            fName: dataTable[i].FName,
            lName: dataTable[i].LName,
            dob: dataTable[i].DOB,
            gender: dataTable[i].Gender,
            licenseAge: dataTable[i].LicenseAge,
            suspension: dataTable[i].Suspension,
            offence: dataTable[i].Offence,
            oType: dataTable[i].OType,
            oYear: dataTable[i].OYear,
            damage: dataTable[i].Damage,
            damageType: dataTable[i].DamageType,
            dYear: dataTable[i].DYear
        };
        motorPage.selectDriverTab();
        if (dataTable[i].PolicyType == "Personal Motor" || dataTable[i].PolicyType == "Commercial Motor" || dataTable[i].PolicyType == "Motorbike") {
            motorPage.addDriver(driver);
        }
        else if (dataTable[i].PolicyType == "Caravan" || dataTable[i].PolicyType == "Motorhome" || dataTable[i].PolicyType == "Trailer") {
            console.log("Entered Caravan Loop");
            console.log(dataTable[i].PolicyType);
            motorPage.selectNext();
            browser.waitForSpinner();
        }
    }
});

When(/^I do Blah blah test$/, async (Table) => {

    //console.log(Table);
    const dataTable = Table.hashes();
    var policyType = dataTable[0].PolicyType;
    console.log("Policy Type ====" + policyType);

});

