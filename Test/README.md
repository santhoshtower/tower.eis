# WebDriverIO Cucumber Framework

This project runs on [WebddriverIO](http://webdriver.io/) with [Cucumber](https://cucumber.io/) to plug [BDD](https://en.wikipedia.org/wiki/Behavior-driven_development) to JavaScript/Typescript. Cucumber maps an ordinary language to code and make sure both the tester and the developer can understand. We are using [Chai](http://www.chaijs.com/api/assert/) as an assertion library and [Autorest](https://github.com/Azure/autorest) tool as an [OpenAPI Specification](https://swagger.io/specification/) code generator for client which then get consumed to perform API testing.

# Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. (note: make sure you have admin rights on your machine)

# Prerequisites

###JRE
Webdriver bindings required java to be installed on the system. They only require Java runtime environment (Make sure JRE is installed on the system).

```
$ java -version   (java version "1.8.0_181" or latest)
```

If not installed:
please download JRE from [here](https://java.com/en/download/windows-64bit.jsp)
 
###NodeJS
The tests code is written in typescript which then get complies to yield JavaScript files. You need [Node]( https://nodejs.org/en/) LTS (install the 8.11.3 LTS or latest version of LTS ) to be installed on your local system for project to work.




# Installation

A step by step series of examples that tell you how to get a Test/development environment running.

##Check out the code

You can use git tools like [Sourcetree](https://www.sourcetreeapp.com/) or [git GUI clients](https://git-scm.com/download/gui/windows) to checkout the code from git repo. Or simple run git command from your windows machine to check it out(note: make sure you have git installed on local machine)

Example:

````
$ git clone https://shaibalkc@bitbucket.org/towerinsurancenz/tower.digital.portal.test.git
````
##Install project dependencies

###note:
 The project can very well work with `npm` so you can install dependencies using command


```
$ cd  project
$ project > npm install
```
But for this project we have used `yarn` as it is faster and offers flat dependency structure as compared to npm's nested structure.

In order make sure the code works on your local machine you first have to to install `yarn`.you  use `npm` to install yarn like:

```
$cd project
$ project > npm i yarn
```


##Installing package dependencies

In order to intall all the node_modules for the project you run following command:
``````
$cd project
$ project > yarn install
``````
### Building test code

To build and compile the code you need to run 
`````
$cd project
$ project > yarn run compile
`````

## Running the tests

To run the test you need to run

````
$cd project
$ project > Yarn run test
````

##Example
The project is the part of first POC.objective of the project is to demonstrate that the framework works properly with the Birds UI and Bird API test.The Bird API and Bird UI were developed as a part fo POC and are example services.

## BDD Examples
The tests written in the `feature` folder are in BDD(Gherkin) format, below is an example:


`````
@ui
`````

````
Feature: Searching for bird page
````
`````
Scenario: I am on welcome page
`````
`````
  Given The user arrives on the bird page
`````
`````
  Then  I should see the bird search box
`````




## Built With

* [WebdriverIO](http://webdriver.io/) - The webdriver binding tool for nodeJS
* [Cucumber](https://cucumber.io/) - BDD Gherking tool
* [Autorest](https://github.com/Azure/autorest) - OpenAPI Specification code generator
* [wdio-multiple-cucumber-html-reporter](https://github.com/wswebcreation/multiple-cucumber-html-reporter/blob/master/docs/WEBDRIVER.IO.MD) - Cucumber html reporting tool for WebdriverIO
* [Chai](http://www.chaijs.com/api/assert/) - Used as an assertion library



### Reporting

We are using [wdio-multiple-cucumber-html-reporter](https://github.com/wswebcreation/multiple-cucumber-html-reporter/blob/master/docs/WEBDRIVER.IO.MD) to create report.Once all tests are executed it will create a html report in `./tmp` folder within the project. Below is a snapshot of the report.
![Snapshot - of the cucumber-html-reporter](./docs/report.jpg?raw=true "cucumber report")

## (Optional)

We are using [Autorest](https://github.com/Azure/autorest) - OpenAPI Specification code generator for our client side REST client code for our API testing.If your project doesnot use swagger then feel free to use other tools such as [Axios](https://github.com/axios/axios) A Promise based HTTP client.


## Author

* **Shaibal Chakraborty**