
const eisRelationshipPageSelector = {

    clickRelationshipTab: '//*[@id="crmForm:crmMainTabsList:1:link"]',
    btnAddRelationship: '//*[@id="crmForm:addRelationshipBtn"]',
    rdBtnNewRltnInd: '//*[@id="crmForm:relationshipType_0:0"]',
    rdBtnNewRltnNonInd: '//*[@id="crmForm:relationshipType_0:1"]',
    dpdwnSalutation: '//*[@id="crmForm:generalInfoLeft_0_salutation"]',
    txtbxFirstName: '//*[@id="crmForm:generalInfoLeft_0_firstName"]',
    txtbxLastName: '//*[@id="crmForm:generalInfoLeft_0_lastName"]',
    dpdwnRelationship: '//*[@id="crmForm:generalInfoRight_0_relationshipRole"]',
    dpdwnReverseRelationship: '//*[@id="crmForm:generalInfoRight_0_reversedRelationshipRole"]',
    txtBoxDOB: '//*[@id="crmForm:generalInfoRight_0_birthDateInputDate"]',
    dpdwnGender: '//*[@id="crmForm:generalInfoRight_0_genderCd"]',
    btnDone: '//*[@id="crmForm:doneBtn_footer"]',
    tabContactsnRelationship: '//*[@id="generalTab:customerSecondTabsList:1:link"]',
    rltnNameVerify: '//*[@id="crmForm:customerContactInfoTogglePanel:header"]/table/tbody/tr/td[2]/div[1]',
    rltnNameVerify1: '//*[@id="crmForm:newRelationshipsTogglePanel_0:header"]/table/tbody/tr/td[2]/div[2]',

    //Currently not using
    btnYes: '//*[@id="searchForm:yes"]',
    txtbxSearchAddress: '//*[@id="crmForm:addressTypeInfo_0_searchAddress_input"]',
    selectAddress: '//*[@id="crmForm:addressTypeInfo_0_searchAddress_panel"]/ul/li[1]',
    btnNext: '//*[@id="crmForm:nextBtn_footer"]',
    lblCustomerName: '//*[@id="custInfoForm:name"]',
    lblAddress: '//*[@id="custInfoForm:j_id_27_4g_5_1_3:addressLine"]',
}

export default eisRelationshipPageSelector;