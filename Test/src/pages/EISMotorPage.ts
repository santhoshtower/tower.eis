import BasePage from './BasePage';
import { limit } from '../../APIService/CustomerCore/models/parameters';

export default class EISMotorPage extends BasePage {
    //Policy Tab
    get txtBoxPolicyCount(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_Policy_policyDetail_policyCount"]') };
    get dpdwnPolicyType(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_Policy_typeOfPolicyCd"]') };
    get dpdwnSource(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_Policy_imported"]') };
    get dpwnDeclarationStatus(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_Policy_policyDetail_declarationStatus"]') };
    get txtBoxEffDate(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_PreconfigAutoPolicyViewOnlyComp_contractTerm_effectiveInputDate"]') };
    get txtBoxExpiryDate(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_PreconfigAutoPolicyViewOnlyComp_contractTerm_expirationInputDate"]') };
    get btnNext(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:next_footer"]') };
    get dpdwnInsuredPartySelection(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:partySelectionPreconfigInsured"]') };
    get txtboxDOBInsured(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_PreconfigInsuredPersonInfoProxy_person_dateOfBirthInputDate"]') };
    get btnValidate(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:validate_PreconfigInsuredPersonAddressContactProxy:validateButton"]') };
    get rdbtnAddress(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:validate_PreconfigInsuredPersonAddressContactProxy:resolvedAddressResults_data"]/tr/td[1]/div/div[2]') };
    get btnOK(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:validate_PreconfigInsuredPersonAddressContactProxy:addressValidationPopupOkBtn"]') };
    get rdBtnPrivacyDeclaration(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_InsuredPrivacyDeclaration_yesNoAnswer:0"]') };
    get rdBtnDisclosureDeclaration(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_InsuredCustomerDisclosureDeclaration_yesNoAnswer:0"]') };
    get rdbtn48HrStandDown(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_Insured48HourStandDown_yesNoAnswer:0"]') };
    get rdbtn48HrStandDown_NO(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_Insured48HourStandDown_yesNoAnswer:1"]') };
    get rdbtnClaimsDeclined(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_InsuredClaimsDeclined_yesNoAnswer:1"]') };
    get rdbtnClaimsDeclined_Yes(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_InsuredClaimsDeclined_yesNoAnswer:0"]') };
    get dpdwnClaimsDeclined(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_InsuredClaimsDeclined_otherAnswer"]') };
    get rdbtnInsuranceDeclined(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_InsuredCarInsuranceDeclined_yesNoAnswer:1"]') };
    get rdbtnCriminalConvictions(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_InsuredCarConvictionQuestion_carConvictionInd:1"]') };
    get rdbtnBankruptcy(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_InsuredCarBankruptcyNoAssetProcedure_yesNoAnswer:1"]') };
    //driver tab
    get tabDriver(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:tabListList_1:3:link"]') };
    get dpdwnDriver(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:partySelectionPreconfigAutoDriver"]') };
    get dpdwnGender(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_PreconfigAutoDriverPersonInfoProxy_person_gender"]') };
    get dpdwnDriverType(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_PreconfigAutoDriver_driverTypeCd"]') };
    get dpdwnDriverLicenseAge(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_PreconfigAutoDriverLicense_howLongHeldYourDriverLicense"]') };
    get rdbtnSpecialCndImp(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_PreconfigAutoDriverLicense_permitBeforeLicenseInd:1"]') };
    get rdbtnSpecialCndImp_Yes(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_PreconfigAutoDriverLicense_permitBeforeLicenseInd:0"]') };
    get rdbtnDriverOffences(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_DriverOffenceQuestion_yesNoAnswer:1"]') };
    get rdbtnDriverOffences_Yes(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_DriverOffenceQuestion_yesNoAnswer:0"]') };
    get dpdwnOffenceType(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_DriverOffence_typeCd"]') };
    get txtBoxOffenceYear(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_DriverOffence_year"]') };

    get rdbtnDriverLoss(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_TwrDriverLossDamageQuestion_yesNoAnswer:1"]') };
    get rdbtnDriverLoss_Yes(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_TwrDriverLossDamageQuestion_yesNoAnswer:0"]') };
    get dpdwnLossType(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_TwrDriverLossDamage_lossDamageType"]') };
    get txtBoxDamageYear(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_TwrDriverLossDamage_lossDamageYear"]') };

    //Additonal Driver
    get btnAddDriver(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:addPreconfigAutoDriver"]') };
    get dpDwnCreateNewDriver(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:partySelectionPreconfigAutoDriver"]') };
    get txtBxDriverFname(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_PreconfigAutoDriverPersonInfoProxy_person_nameInfo_firstName"]') };
    get txtBxDriverLname(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_PreconfigAutoDriverPersonInfoProxy_person_nameInfo_lastName"]') };
    get dpdwnDriverGender(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_PreconfigAutoDriverPersonInfoProxy_person_gender"]') };
    get txtBoxAddDriverDob(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_PreconfigAutoDriverPersonInfoProxy_person_dateOfBirthInputDate"]') };
    //Vehicle Tab
    get btnAddVehicle(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:addPreconfigVehicleVer2"]') };
    get dpdwnVehicleUse(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_PreconfigVehicleVer2_ratingInfo_vehicleUsageCd"]') };
    get txtbxRego(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_PreconfigVehicleVer2_baseInfo_registrationNo"]') };
    get dpdwnMake(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_PreconfigVehicleVer2_baseInfo_manufacturer"]') };
    get dpdwnModel(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_PreconfigVehicleVer2_baseInfo_model"]') };
    get dpdwnYear(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_PreconfigVehicleVer2_baseInfo_manufactureYear"]') };
    get dpdwnStyle(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_PreconfigVehicleVer2_baseInfo_vehBodyTypeCd"]') };
    get dpdwnType(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_PreconfigVehicleVer2_baseInfo_series"]') };

    // Caravan , Motorhome, trailer
    get rdbtnRnW(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_PreconfigVehicleVer2_ratingInfoEntity_nonSymbolPickup:0"]') };
    get dpdwnLength(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_PreconfigVehicleVer2_useStatusCd"]') };
    get txtbxYear(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_PreconfigVehicleVer2_baseInfo_otherModel"]') };
    get txtBxSumInsured(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_PreconfigVehicleVer2_ratingInfo_costNew"]') };
    get rdbtnForBusinessUse(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_PreconfigVehusedinComBuzOperationQuestionAnswer_yesNoAnswer:0"]') };
    get rdbtnForPersonalUse(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_PreconfigVehusedinComBuzOperationQuestionAnswer_yesNoAnswer:1"]') };
    get dpdwnDistance(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_VehicleUseDetailComponent_ratingInfoEntity_declaredAnnualMiles"]') };
    get rdbtnVehicleuse(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_PreconfigVehusedinComBuzOperationQuestionAnswer_yesNoAnswer:1"]') };
    get dpdwnParking(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_VehicleAdditionalInformationComponent_parkingLocationCd"]') };
    get rdbtnOwnership(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_PreconfigGarageParkedQuestionAnswer_yesNoAnswer:0"]') };
    get dpdwnPaymentPlan(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_TwrPolicyPaymentPlan_paymentPlanType"]') };
    get dpdwnPlan(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_PreconfigVehiclePackageManager_packageCd"]') };
    get dpdwnExcess(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_PreconfigVehicleRiskDeductible_deductibleAmount"]') };
    get btnCalcPremium(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:actionButton_PrecconfigAutoPremiumCalculationAction"]') };
    get btnSave(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="topSaveLink"]') };
    get btnSaveNexit(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="topSaveAndExitLink"]') };
    get rdbtnSameRiskAddress(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_VehicleGaragingComponent_sameAsGarageAddressInd:1"]') };
    get tabVehicle(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:tabListList_1:3:linkLabel"]') };
    get txtBxSearchAddress(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:VehicleGaragingAddressInfo_addressQueryStr_input"]') };
    get selectAddress1(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:VehicleGaragingAddressInfo_addressQueryStr_panel"]/table/tbody/tr[1]') };
    get selectAddress2(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('.ui-corner-all.ui-state-highlight > td') };
    get btnErrorCancel(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="errorsForm:back"]') };
    get tabPremiumNCoverages(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:tabListList_1:6:linkLabel"]') };
    get btnPurchase(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:purchaseQuote_footer"]') };
    get btnPurchaseYesPopup(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:okBtn"]') };
    get btnFinish(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="purchaseForm:yesButton_footer"]'); }
    get lblPolicyNo(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="productConsolidatedViewForm:policyConsolidatedInfoTable:0:navigateToPolicyTab"]'); }
    get dpdwnTrailerDesc(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_PreconfigVehicleVer2_baseInfo_otherSeries"]'); }
    get txtCaravanYearMakeModel(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_PreconfigVehicleVer2_baseInfo_otherSeries"]'); }
    get txtBxTrailerYear(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_PreconfigVehicleVer2_baseInfo_otherManufacturer"]'); }
    get dpdwnLimit(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="policyDataGatherForm:sedit_VehicleSignwritingCoverageCoverage_limitAmount"]'); }

    public setCoverages(limit): any {
        //throw new Error("Method not implemented."); 
        this.dpdwnLimit.selectByVisibleText(limit);
        browser.waitForSpinner();
    }
    public selectClaimsDeclinedReason(CDReason: any): any {
        this.dpdwnClaimsDeclined.selectByVisibleText(CDReason);
        browser.waitForSpinner();
    }

    public selectInsuranceDeclined(): any {
        this.rdbtnInsuranceDeclined.click();
        browser.waitForSpinner();
    }

    public selectCriminalConvictions(): any {
        this.rdbtnCriminalConvictions.click();
        browser.waitForSpinner();
    }

    public selectBankruptcy(): any {
        this.rdbtnBankruptcy.scroll(0, 1000);
        browser.waitForSpinner();
        this.rdbtnBankruptcy.click();
    }

    public selectClaimsDeclined(hasBeenDeclined): any {
        if (hasBeenDeclined) {
            this.rdbtnClaimsDeclined_Yes.click();
        } else {
            this.rdbtnClaimsDeclined.click();
        }
        browser.waitForSpinner();
    }

    public selectInsuredParty(fullName: any): any {
        this.dpdwnInsuredPartySelection.selectByVisibleText(fullName);
        browser.waitForSpinner();
    }

    public acceptDeclarations(privacy, disclosure, standDown): any {
        if (privacy === true) {
            this.rdBtnPrivacyDeclaration.click();
            browser.waitForSpinner();
        }
        if (disclosure === true) {
            this.rdBtnDisclosureDeclaration.click();
            browser.waitForSpinner();
        } else {

        }
        if (standDown === true) {
            this.rdbtn48HrStandDown.click();
            browser.waitForSpinner();
        } else {
            this.rdbtn48HrStandDown_NO.click();
            browser.waitForSpinner();
        }
    }

    public setPolicyCount(PolicyCount: any): any {
        this.txtBoxPolicyCount.setValue(PolicyCount);
        browser.keys("Tab");
        browser.waitForSpinner();
        this.btnNext.click();
        browser.waitForSpinner();
    }

    public selectPolicyType(PolicyType: any): any {
        this.dpdwnPolicyType.selectByVisibleText(PolicyType);
        browser.waitForSpinner();
        console.log(this.dpdwnSource.getValue());
    }


    public addDriver(driver) {
        browser.waitForSpinner();
        this.btnAddDriver.click();
        browser.waitForSpinner();
        this.dpdwnDriver.selectByVisibleText("Create New Driver");
        browser.waitForSpinner();
        this.dpdwnDriverType.selectByVisibleText(driver.type);
        browser.waitForSpinner();
        this.txtBxDriverFname.setValue(driver.fName);
        browser.keys("Tab");
        browser.waitForSpinner();
        this.txtBxDriverLname.setValue(driver.lName);
        browser.keys("Tab");
        browser.waitForSpinner();
        this.txtBoxAddDriverDob.setValue(driver.dob);
        browser.keys("Tab");
        browser.waitForSpinner();
        this.dpdwnGender.selectByVisibleText(driver.gender);
        browser.waitForSpinner();
        this.dpdwnDriverLicenseAge.selectByVisibleText(driver.licenseAge);
        browser.waitForSpinner();
        this.rdbtnSpecialCndImp.click();
        browser.waitForSpinner();
        console.log(driver.suspension);
        if (driver.offence == "Yes") {
            this.rdbtnDriverOffences_Yes.click();
            browser.waitForSpinner();
            this.dpdwnOffenceType.selectByVisibleText(driver.oType);
            browser.waitForSpinner();
            this.txtBoxOffenceYear.setValue(driver.oYear);
            browser.keys("Tab");
            browser.waitForSpinner();
        } else {
            this.rdbtnDriverOffences.click();
        }
        browser.waitForSpinner();
        if (driver.damage == "Yes") {
            this.rdbtnDriverLoss_Yes.scroll(0, 500);
            this.rdbtnDriverLoss_Yes.click();
            browser.waitForSpinner();
            this.dpdwnLossType.scroll(0, 500);
            this.dpdwnLossType.selectByVisibleText(driver.damageType);
            browser.waitForSpinner();
            this.txtBoxDamageYear.setValue(driver.dYear);
        } else {
            this.rdbtnDriverLoss.click();
        }

        browser.waitForSpinner();
        this.btnNext.click();
        browser.waitForSpinner();
    }


    public selectDriverTab(): any {
        this.tabDriver.click();
        browser.waitForSpinner();
    }

    public evenMoreJunkStuff_Temp(): any {
        browser.waitForSpinner();
        this.btnCalcPremium.click();
        browser.waitForSpinner();
        this.btnNext.click();
        console.log("btnNext clicked...")
        browser.waitForSpinner();
        this.btnSave.click();
        browser.waitForSpinner();
        this.btnPurchase.click();
        browser.waitForSpinner();
        this.btnPurchaseYesPopup.click();
        console.log("Yes clicked...");
    }

    public moreJunkStuff_Temp(): any {
        browser.waitForSpinner();
        this.btnCalcPremium.click();
        browser.waitForSpinner();
        this.btnNext.click();
        console.log("btnNext clicked...")
        browser.waitForSpinner();
        this.btnPurchase.click();
        console.log("btnPurchase clicked...")
        browser.waitForSpinner();
        this.btnPurchaseYesPopup.click();
        console.log("Yes clicked...")
        browser.waitForSpinner();
        this.btnFinish.click();
        console.log("Finish clicked...")
        browser.waitForSpinner();
    }

    public junkStuff_Temp(address: string): any {
        this.btnErrorCancel.click();
        browser.waitForSpinner();
        this.tabVehicle.click();
        browser.waitForSpinner();
        this.rdbtnSameRiskAddress.scroll(0, 1000);
        this.rdbtnSameRiskAddress.click();
        browser.waitForSpinner();
        this.txtBxSearchAddress.setValue(address);
        browser.waitForSpinner();
        browser.pause(1500);
        this.selectAddress1.click();
        browser.waitForSpinner();
        this.tabPremiumNCoverages.scroll(0, 1000);
        this.tabPremiumNCoverages.click();
        browser.waitForSpinner();
    }

    public finishQuote(): any {
        this.btnPurchase.click();
        browser.waitForSpinner();
        this.btnPurchaseYesPopup.click();
        browser.waitForSpinner();
    }

    public calculatePremium(): any {
        this.btnCalcPremium.click();
        browser.waitForSpinner();
        browser.pause(500);
        this.btnNext.click();
        browser.waitForSpinner();
    }

    public selectExcessByIndex(arg0: number): any {
        this.dpdwnExcess.selectByIndex(1);
        browser.waitForSpinner();
        browser.pause(1000);
    }

    public selectPlan(Plan) {
        this.dpdwnPaymentPlan.selectByIndex(1);
        browser.waitForSpinner();
        this.dpdwnPlan.selectByVisibleText(Plan);
        console.log("Plan selected");
        browser.waitForSpinner();
        // if (Plan == "Comprehensive Market") {
        //     console.log("select plan again?");
        //     this.dpdwnLimit.selectByIndex(1);
        //     console.log("Plan selected again");
        //     browser.waitForSpinner();
        // }
    }

    public addVehicleUse(usage: string): any {
        this.dpdwnVehicleUse.selectByVisibleText(usage);
        browser.waitForSpinner();
    }

    private addRego(rego) {
        this.txtbxRego.setValue(rego);
        console.log("rego entered");
        this.selectEnter();
    }
    private addMake(make) {
        console.log("add make");
        this.dpdwnMake.selectByVisibleText(make);
        console.log("make selected");
        browser.waitForSpinner();
    }
    private addModel(model) {
        console.log("add model");
        this.dpdwnModel.selectByVisibleText(model);
        console.log("model entered");
        browser.waitForSpinner();
    }
    private addType(type) {
        this.dpdwnType.selectByVisibleText(type);
        browser.waitForSpinner();
    }
    private addStyle(style) {
        this.dpdwnStyle.selectByVisibleText(style);
        browser.waitForSpinner();
    }
    private addDistance(distance) {
        this.dpdwnDistance.selectByVisibleText(distance);
        browser.waitForSpinner();
    }

    private addYear(year) {
        // browser.pause(60000);
        console.log("set year: ", year);
        this.dpdwnYear.selectByVisibleText(year);
        console.log("year entered");
        browser.waitForSpinner();
        this.selectEnter();
    }

    private addTrailerYear(year) {
        console.log("set year: ", year);
        this.txtBxTrailerYear.setValue(year);
        console.log("year entered");
        browser.waitForSpinner();
        this.selectEnter();
    }

    private addSumInsured(amount) {
        browser.waitForSpinner();
        browser.pause(2000);
        this.txtBxSumInsured.setValue(amount);
        browser.waitForSpinner();
        this.selectEnter();
    }

    private selectEnter() {
        console.log("Selecting enter");
        browser.keys("Enter");
        browser.waitForSpinner();
        console.log("eneter clicked");
    }

    public selectSave(): any {
        this.btnSave.click();
        browser.waitForSpinner();
    }

    private addAddress(parkingLocation, address) {
        this.dpdwnParking.scroll(0, 1000);
        console.log("Parking location: ", parkingLocation);
        this.dpdwnParking.selectByVisibleText(parkingLocation);
        browser.waitForSpinner();
        this.rdbtnOwnership.click();
        browser.waitForSpinner();
        this.txtBxSearchAddress.setValue(address);
        browser.pause(500);
        browser.waitForSpinner();
        browser.pause(500);
        this.selectAddress1.click();
        browser.pause(500);
        browser.waitForSpinner();
        this.btnNext.click();
    }

    public addCaravan(vehicleDetail, address, distance?): any {
        this.btnAddVehicle.click();
        this.addRego(vehicleDetail.Rego);
        this.rdbtnRnW.click();
        browser.waitForSpinner();
        this.dpdwnLength.selectByIndex(1);
        browser.waitForSpinner();
        this.addYearMakeModel(vehicleDetail.yearMakeModel);
        console.log("Caravan value set");
        this.addSumInsured(vehicleDetail.SumInsured);
        console.log("Caravan sum insured set");
        this.rdbtnVehicleuse.scroll(0, 500);
        this.rdbtnVehicleuse.click();
        browser.waitForSpinner();

        if (distance !== undefined) {
            console.log("Caravan setting distance");
            this.addDistance(distance);
            console.log("Caravan distance set");
        }
        this.addAddress(vehicleDetail.Parking, address);
    }

    private addYearMakeModel(year): any {
        console.log("Caravan year make model: ", year);
        this.txtbxYear.setValue(year);
        browser.waitForSpinner();
        this.selectEnter();
    }

    public addTrailer(vehicleDetail, address, distance?): any {
        this.btnAddVehicle.click();
        this.addRego(vehicleDetail.Rego);
        //browser.pause(40000);
        this.rdbtnRnW.click();
        browser.waitForSpinner();
        this.addDescription(vehicleDetail.Description);
        this.addTrailerYear(vehicleDetail.Year);
        this.addSumInsured(vehicleDetail.SumInsured);
        this.rdbtnVehicleuse.scroll(0, 500);
        this.rdbtnVehicleuse.click();
        browser.waitForSpinner();
        // if (distance !== undefined) {
        //     this.addDistance(vehicleDetail.Distance);
        // }
        this.addAddress(vehicleDetail.Parking, address);
    }
    public addDescription(description) {
        console.log("Adding description: ", description);
        this.dpdwnTrailerDesc.selectByVisibleText(description);
        browser.waitForSpinner();
    }

    public addPersonalVehicle(vehicleDetail, address, vehicleUse?): any {
        console.log("before click add cehicle accepted");
        this.btnAddVehicle.click();
        console.log("add vehicle clicked");
        this.addRego(vehicleDetail.Rego);
        this.addMake(vehicleDetail.Make);
        this.addModel(vehicleDetail.Model);
        this.addYear(vehicleDetail.Year);
        this.addStyle(vehicleDetail.Style);
        this.addType(vehicleDetail.Type);
        this.addDistance(vehicleDetail.Distance);
        console.log("vehicle use: ", vehicleUse);
        if (vehicleUse !== undefined && vehicleUse !== "") {
            console.log("adding vehicle use");
            this.addVehicleUse(vehicleUse);
            console.log("added vehicle use");
        }

        this.addAddress(vehicleDetail.Parking, address);
    }

    public addCommercialVehicle(vehicleDetail, address, vehicleUse): any {
        console.log("before click add cehicle accepted");
        this.btnAddVehicle.click();
        console.log("add vehicle clicked");
        this.addRego(vehicleDetail.Rego);
        this.addMake(vehicleDetail.Make);
        this.addModel(vehicleDetail.Model);
        this.addYear(vehicleDetail.Year);
        this.addStyle(vehicleDetail.Style);
        this.addType(vehicleDetail.Type);
        this.addDistance(vehicleDetail.Distance);
        console.log("adding vehicle use");
        this.addVehicleUse(vehicleUse);
        console.log("added vehicle use");
        this.addAddress(vehicleDetail.Parking, address);
    }

    public addMotorBike(vehicleDetail, address, businessUse): any {
        console.log("before click add cehicle accepted");
        this.btnAddVehicle.click();
        console.log("add vehicle clicked");
        this.addRego(vehicleDetail.Rego);
        this.addMake(vehicleDetail.Make);
        this.addModel(vehicleDetail.Model);
        this.addYear(vehicleDetail.Year);
        this.addStyle(vehicleDetail.Style);
        this.addType(vehicleDetail.Type);
        this.addDistance(vehicleDetail.Distance);

        if (businessUse === true) {
            console.log("setting is for business use");
            this.rdbtnForBusinessUse.click();
            console.log("business use applied");
            browser.waitForSpinner();
        }
        else {
            this.rdbtnForPersonalUse.click();
            browser.waitForSpinner();
        }

        this.addAddress(vehicleDetail.Parking, address);
    }

    public selectNext(): any {
        this.btnNext.click();
        browser.waitForSpinner();
    }

    public enterDriverInfo(fullName, gender, driverType, DriverAge): any {
        this.btnNext.click();
        console.log("Clicked on Next")
        browser.waitForSpinner();
        this.dpdwnDriver.selectByVisibleText(fullName);
        browser.waitForSpinner();
        this.dpdwnGender.selectByVisibleText(gender);
        browser.waitForSpinner();
        this.dpdwnDriverType.selectByVisibleText(driverType);
        browser.waitForSpinner();
        this.dpdwnDriverLicenseAge.selectByVisibleText(DriverAge);
        browser.waitForSpinner();
        this.rdbtnSpecialCndImp.click();
        browser.waitForSpinner();
        this.rdbtnDriverOffences.click();
        browser.waitForSpinner();
        this.rdbtnDriverLoss.click();
        browser.waitForSpinner();
        this.btnNext.click();
        browser.waitForSpinner();
    }

    public enterPolicyInformation(fullName, dob): any {
        this.btnNext.click();
        this.rdBtnPrivacyDeclaration.click();
        browser.waitForSpinner();
        this.rdBtnDisclosureDeclaration.click();
        browser.waitForSpinner();
        this.rdbtn48HrStandDown.click();
        browser.waitForSpinner();
        this.dpdwnInsuredPartySelection.selectByVisibleText(fullName);
        browser.waitForSpinner();
        this.txtboxDOBInsured.setValue(dob);
        browser.keys("Enter");
        browser.waitForSpinner();
        this.btnValidate.click();
        browser.waitForSpinner();
        this.rdbtnAddress.click();
        browser.waitForSpinner();
        this.btnOK.click();
        browser.waitForSpinner();
        this.rdbtnClaimsDeclined.click();
        browser.waitForSpinner();
        this.rdbtnInsuranceDeclined.click();
        browser.waitForSpinner();
        this.rdbtnCriminalConvictions.click();
        browser.waitForSpinner();
        this.rdbtnBankruptcy.scroll(0, 1000);
        browser.waitForSpinner();
        this.rdbtnBankruptcy.click();
        browser.waitForSpinner();
        //console.log("Clicked before Next")
    }
    public selectFinish() {
        this.btnFinish.click();
    }
}
