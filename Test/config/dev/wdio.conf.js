var deepAssign = require('deep-assign');
var commonConfig = require('./../_common');
var devConfig = require('./_dev');
var customConfig = require('./_custom');

var config = deepAssign(commonConfig, devConfig, customConfig);
exports.config = config;
