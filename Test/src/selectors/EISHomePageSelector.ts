
const eisHomePageSelector = {

    clickCustomerTab: '//*[@id="tabForm:topTabsBarList:1:linkLabel"]',
    clickCreateCustomer: '//*[@id="searchForm:createAccountBtnAlway"]',
    rdBtnCreateIndiviudal: '//*[@id="searchForm:customerType:0"]',
    rdBtnCreateNonIndiviudal: '//*[@id="searchForm:customerType:1"]',
    btnYes: '//*[@id="searchForm:yes"]',
    txtbxFirstName: '//*[@id="crmForm:generalInfo_firstName"]',
    txtbxLastName: '//*[@id="crmForm:generalInfo_lastName"]',
    txtbxSearchAddress: '//*[@id="crmForm:addressTypeInfo_0_addressQueryStr_input"]',
    selectAddress: '//*[@id="crmForm:addressTypeInfo_0_addressQueryStr_panel"]/table/tbody/tr[1]',
    btnNext: '//*[@id="crmForm:nextBtn_footer"]',
    btnDone: '//*[@id="crmForm:doneBtn_footer"]',
    lblCustomerName: '//*[@id="custInfoForm:name"]',
    lblAddress: '//*[@id="custInfoForm:j_id_27_58_1_3:addressLine"]',
    txtBoxDOB: '//*[@id="crmForm:generalInfo_birthDateInputDate"]',
    dpdwnSalutation: '//*[@id="crmForm:generalInfo_salutation"]',
    dpdwnGender: '//*[@id="crmForm:generalInfo_genderCd"]',
    btnAddQuote: '//*[@id="quotes:addPolicyBtn"]',
    rdBtnPaperless: '//*[@id="crmForm:additionalInfo_paperless"]',

    // non indi elements:
    dpdwnNonIndiType: '//*[@id="crmForm:generalInfoLeft_businessType"]',
    txtBoxNameLegal: '//*[@id="crmForm:generalInfoLeft_legalName"]',
    txtBoxCompanyNo: '//*[@id="crmForm:generalInfoLeft_legalId"]',

    //Airpoints into action.
    btnAddGroup: '//*[@id="crmForm:groupInfoMethod:addGroupInfoEvent"]',
    btnGroupSearch: '//*[@id="crmForm:showGroupSearchPopup_0"]',
    btnPopupSearch: '//*[@id="assignGroupInfoForm:groupSearchBtn"]',
    linkAirpointsSelect: '//*[@id="assignGroupInfoForm:searchResultTable:0:showGroupSearchPopup"]',
    txtBxAPFname: '//*[@id="crmForm:generalInfo_0_membershipFirstName"]',
    txtBxARLName: '//*[@id="crmForm:generalInfo_0_membershipLastName"]',
    txtBxAirpointsNo: '//*[@id="crmForm:generalInfo_0_membershipNumber"]',

    // Relationship Page Elements
    rdbtnRelationShipType_Ind: '//*[@id="crmForm:relationshipType_0:0"]',
    rdbtnRelationShipType_NonInd: '//*[@id="crmForm:relationshipType_0:1"]',

}

export default eisHomePageSelector;