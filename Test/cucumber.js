var deepAssign = require('deep-assign');

// get environment settings
var envSetting = require(`./config/${process.env.ENVIRONMENT}/cucumber`)

// common settings for all envionments
const commonSetting = {
    requiredModule: '--require-module ts-node/register',
    logLevel: '--logLevel:error',
    reportPath: '-f json:tmp/api-test-result.json',
}


var config = deepAssign(commonSetting, envSetting);

// generate settings to command line style
const toCommandLine = function (config) {
    let command = '';
    for (let key in config) {
        command += `${config[key]} `;
    }
    return command;
}

module.exports = {
    default: toCommandLine(config)
};
