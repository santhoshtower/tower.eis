const eisBillingPageSelector = {

    chkBoxCreateNewAcc: '//*[@id="purchaseForm:billingAccount_createNewAccount"]',
    dpdwnDuedayType: '//*[@id="purchaseForm:accountDueDayComponent_dueDayType"]',
    dpdwnBillingState: '//*[@id="purchaseForm:billingAccount_billingAccountDetails_billingAccountAddress_stateProvCd"]',
    btnBillingValidate: '//*[@id="purchaseForm:billingAccount_billingAccountDetails_billingAccountAddress_addressValidate:validateButton"]',
    rdbtnResolvedaddress: '//*[@id="purchaseForm:billingAccount_billingAccountDetails_billingAccountAddress_addressValidate:resolvedAddressResults_data"]/tr/td[1]/div/div[2]',
    btnBillingPopupOk: '//*[@id="purchaseForm:billingAccount_billingAccountDetails_billingAccountAddress_addressValidate:addressValidationPopupOkBtn"]',
    btnAddPaymentMthd: '//*[@id="purchaseForm:addPaymentMethodBtn"]',
    chkBoxEnableRecurringPayment_Policy: '//*[@id="purchaseForm:billablePolicyTermRecurringPayments_billingAutomaticRecurring"]',
    dpdwnUsePaymentMthd_Policy: '//*[@id="purchaseForm:billablePolicyTermRecurringPayments_paymentType"]',
    chkBoxEnableRecurringPayment_Billing: '//*[@id="purchaseForm:billingAcccountRecurringPayments_billingAutomaticRecurring"]',
    dpdwnUsePaymentMthd_Billing: '//*[@id="purchaseForm:billingAcccountRecurringPayments_paymentType"]',
    dpdwnPaymentPlan: '//*[@id="purchaseForm:paymentPlanSelectMenu"]',

    // payment method page
    dpdwnPaymentMethod: '//*[@id="paymentMethodTypeFormSwitch:paymentMethodType"]',
    dpdwnPaymentMthdState: '//*[@id="paymentMethodForm:addressPaymentMethod_billableAddres_stateProvCd"]',
    btnPaymentMthdValidate: '//*[@id="paymentMethodForm:addressValidate:validateButton"]',
    rdbtnPyamentMthdResolvedAddress: '//*[@id="paymentMethodForm:addressValidate:resolvedAddressResults_data"]/tr/td[1]/div/div[2]',
    btnPaymentMthdPopUpOK: '//*[@id="paymentMethodForm:addressValidate:addressValidationPopupOkBtn"]',

    //Credit Card information
    dpdwnCardType: '//*[@id="paymentMethodForm:generalPaymentMethod_displayType"]',
    txtBoxCCHName: '//*[@id="paymentMethodForm:generalPaymentMethod_fullName"]',
    txtBoxCCNo: '//*[@id="paymentMethodForm:generalPaymentMethod_fullNumber"]',
    txtBoxVN: '//*[@id="paymentMethodForm:generalPaymentMethod_securityCode"]',
    dpdwnCCExpMonth: '//*[@id="paymentMethodForm:expMonth"]',
    dpdwnCCExpYear: '//*[@id="paymentMethodForm:expYear"]',
    btnAddorUpdate: '//*[@id="paymentMethodForm:saveBtn"]',
    btnAddorUpdate_EFT: '//*[@id="paymentMethodEFTForm:saveBtn"]',
    btnBackFooter: '//*[@id="primaryButtonsForm:backButton_footer"]',

    //Direct Debit Information.
    txtBoxBankID: '//*[@id="paymentMethodEFTForm:paymentEFT_bankID"]',
    txtBoxBranch: '//*[@id="paymentMethodEFTForm:paymentEFT_bankBranch"]',
    txtBoxBaseAccNo: '//*[@id="paymentMethodEFTForm:paymentEFT_accountBaseNumber"]',
    txtBoxAccSuffix: '//*[@id="paymentMethodEFTForm:paymentEFT_accountSuffix"]',
    txtBoxBankName: '//*[@id="paymentMethodEFTForm:paymentEFT_bankName"]',
    btnBankValidate: '//*[@id="paymentMethodEFTForm:moduloValidate"]',
    dpdwnpaymentMethodToBeUsedFor: '//*[@id="paymentMethodEFTForm:paymentMethodToBeUsedFor"]',
    rdbtnAuthRx: '//*[@id="paymentMethodEFTForm:authorisationReceived:0"]',
    rdbtnFormSign: '//*[@id="paymentMethodEFTForm:setupDirectDebitAuthorityWithoutSigningForm:0"]',
    rdbtn9months: '//*[@id="paymentMethodEFTForm:cancelledDirectDebitAuthorityWithTowerInsurance:0"]',


}
export default eisBillingPageSelector;