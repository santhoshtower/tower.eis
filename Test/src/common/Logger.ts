import { isUndefined } from "util";

export class Logger {
    static logData;

    static reset() {
        this.logData = "";
    }

    static log(prefix, stringToLog?) {
        const util = require('util');
        if (this.logData === undefined) {
            this.logData = util.inspect(prefix);
            if (stringToLog !== undefined) {
                this.logData += util.inspect(stringToLog) + "<br/>";
            }
        } else {
            this.logData += util.inspect(prefix) + "<br/>";
            if (stringToLog !== undefined) {
                this.logData += util.inspect(stringToLog) + "<br/>";
            }
        }

    }

    static getLog(): string {
        if (this.logData === undefined) {
            return "";
        }
        return this.logData.toString();
    }
}
