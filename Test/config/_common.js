module.exports = {

    sync: true,

    logLevel: 'verbose',

    coloredLogs: true,

    screenshotPath: './errorShots/',

    baseUrl: 'http://tmitest.surge.sh/',

    waitforTimeout: 10000,

    connectionRetryTimeout: 90000,

    connectionRetryCount: 3,

    services: ['selenium-standalone'],

    framework: 'cucumber',

    reporters: ['spec', 'multiple-cucumber-html'],

    reporterOptions: {
        htmlReporter: {
            jsonFolder: './tmp',
            reportFolder: './tmp/report',
            displayDuration: true,
            durationInMS: true,
            disableLog: true,
        }
    },

    cucumberOpts: {
        // <boolean> show full backtrace for errors
        backtrace: false,
        // <string[]> filetype:compiler used for processing required features
        compiler: ['ts:ts-node/register'],
        // <boolean< Treat ambiguous definitions as errors
        failAmbiguousDefinitions: true,
        // <boolean> invoke formatters without executing steps
        // dryRun: false,
        // <boolean> abort the run on first failure
        failFast: false,
        // <boolean> Enable this config to treat undefined definitions as
        // warnings
        ignoreUndefinedDefinitions: false,
        // <string[]> ("extension:module") require files with the given
        // EXTENSION after requiring MODULE (repeatable)
        name: [],
        // <boolean> hide step definition snippets for pending steps
        snippets: true,
        // <boolean> hide source uris
        source: true,
        // <string[]> (name) specify the profile to use
        profile: [],
        // <string[]> (file/dir) require files before executing features
        require: ['./src/common/Commands.ts', './src/steps/UI/*.ts'],
        // <string> specify a custom snippet syntax
        snippetSyntax: undefined,
        // <boolean> fail if there are any undefined or pending steps
        strict: true,

        tags: [],
        // <string> (expression) only execute the features or scenarios with
        // tags matching the expression, see
        // https://docs.cucumber.io/tag-expressions/
        // <boolean> add cucumber tags to feature or scenario name
        tagsInTitle: false,
        // <number> timeout for step definitions
        timeout: 60000,
    },
    afterScenario: function (scenario) {
        // deleting session storage data after each UI scenario
        if (scenario.tags) {
            let isUiScenario = false;
            scenario.tags.forEach(function (tag) {
                if (tag.name.includes('ui')) {
                    isUiScenario = true;
                }
            });
            if (isUiScenario) {
                browser.sessionStorage('DELETE', 'persist:root')
            }
        }
    },
    afterStep: function (stepResult) {
        if (stepResult.status === 'failed') {
            // log page source
            //console.log(browser.getSource());
        }
    },
    afterSession: async function () {
        // workaround to make sure the chromedriver shuts down
        await browser.end().pause(1000);
    },
    after: async function () {
        // workaround to make sure the chromedriver shuts down
        await browser.pause(1000);
    }
};