@ui
Feature: All House Scenarios Including billing

    Background: Create an Individual Customer without Relationship
        Given I Login into the EIS SIT
        And I create a Customer without Relationship
        Then I validate the Customer Details "without" Relationship


    # Scenario Outline: Create a New HnC Policy for a House
    #     Given I create the "<PolicyType>" Policy for the Customer
    #     When I enter a "<PolicyType>" , "<PolicyStyle>" Policy information and Policy Count "<PolicyCount>"
    #     And I enter the "<PolicyType>" Insured information
    #     And I enter the "<PolicyStyle>" Details
    #     And I enter the Driver information for "<PolicyStyle>"
    #     And I enter the FormsAddl info
    #     And I choose the plan "<Plan>" Premium&Coverages for "<PolicyStyle>"
    #     And I Finish Quote in Document Request Tab
    #     And I create a new billing account with "<BillingPeriod>"
    #     ####values include:Monthly,Weekly
    #     And I Create a Payment Method of "<PaymentType>"
    #     ####Payment method values: CreditCard,PCI,DirectDebit
    #     And I enable "<PaymentOptions>" Payment Options
    #     ####payment options include: Policy,Billing,both
    #     And I choose the Paymentplan to "<PaymentPlan>"
    #     ####payment plan eligible values: SixMonthly,Quarterly,Monthly,Annual
    #     Then I Purchase the policy by clicking on finish
    #     Examples:
    #         | PolicyType | PolicyStyle | PolicyCount | Plan                 | BillingPeriod | PaymentType | PaymentOptions | PaymentPlan |
    #         | HnC        | House       | 1           | Comprehensive Market | Monthly       | CreditCard  | both           | Annual      |
    #         | HnC        | House       | 1           | Comprehensive Market | Monthly       | DirectDebit | both           | Annual      |
    #         | HnC        | House       | 1           | Comprehensive Market | Weekly        | CreditCard  | both           | Annual      |
    #         | HnC        | House       | 1           | Comprehensive Market | Weekly        | DirectDebit | both           | Annual      |
    #         | HnC        | House       | 1           | Comprehensive Market | Monthly       | CreditCard  | both           | SixMonthly  |
    #         | HnC        | House       | 1           | Comprehensive Market | Monthly       | DirectDebit | both           | SixMonthly  |
    #         | HnC        | House       | 1           | Comprehensive Market | Weekly        | DirectDebit | both           | SixMonthly  |
    #         | HnC        | House       | 1           | Comprehensive Market | Weekly        | CreditCard  | both           | SixMonthly  |
    #         | HnC        | House       | 1           | Comprehensive Market | Monthly       | DirectDebit | both           | Quarterly   |
    #         | HnC        | House       | 1           | Comprehensive Market | Monthly       | CreditCard  | both           | Quarterly   |
    #         | HnC        | House       | 1           | Comprehensive Market | Weekly        | DirectDebit | both           | Quarterly   |
    #         | HnC        | House       | 1           | Comprehensive Market | Weekly        | CreditCard  | both           | Quarterly   |
    #         | HnC        | House       | 1           | Comprehensive Market | Monthly       | DirectDebit | both           | Monthly     |
    #         | HnC        | House       | 1           | Comprehensive Market | Monthly       | CreditCard  | both           | Monthly     |
    #         | HnC        | House       | 1           | Comprehensive Market | Weekly        | DirectDebit | both           | Monthly     |
    #         | HnC        | House       | 1           | Comprehensive Market | Weekly        | CreditCard  | both           | Monthly     |

    Scenario: Create a New House Policy for a Customer with Fortnight-Weekly, DD, Annual, Billing details
        Given I create the "HnC" Policy for the Customer
        When I enter a "HnC" , "House" Policy information and Policy Count "1"
        And I enter the "HnC" Insured information
        # And I do Junk stuff in HNC
        And I enter details in Risk Address Tab
        And I enter details in CoveragesandPremium
        And I issue the Policy
        And I create a new billing account with "Weekly"
        #values include:Monthly,Weekly
        And I Create a Payment Method of "DirectDebit"
        #Payment method values: CreditCard,PCI,DirectDebit
        And I enable "both" Payment Options
        #payment options include: Policy,Billing,both
        # And I choose the Paymentplan to "Annual"
        #payment plan eligible values: SixMonthly,Quarterly,Monthly,Annual
        Then I Purchase the policy by clicking on finish

    Scenario: Create a New House Policy for a Customer with Fortnight-Weekly, CreditCard, Annual, Billing details
        Given I create the "HnC" Policy for the Customer
        When I enter a "HnC" , "House" Policy information and Policy Count "1"
        And I enter the "HnC" Insured information
        # And I do Junk stuff in HNC
        And I enter details in Risk Address Tab
        And I enter details in CoveragesandPremium
        And I issue the Policy
        And I create a new billing account with "Weekly"
        #values include:Monthly,Weekly
        And I Create a Payment Method of "CreditCard"
        #Payment method values: CreditCard,PCI,DirectDebit
        And I enable "both" Payment Options
        #payment options include: Policy,Billing,both
        # And I choose the Paymentplan to "Annual"
        #payment plan eligible values: SixMonthly,Quarterly,Monthly,Annual
        Then I Purchase the policy by clicking on finish

    Scenario: Create a New House Policy for a Customer with Monthly, DD, Annual, Billing details
        Given I create the "HnC" Policy for the Customer
        When I enter a "HnC" , "House" Policy information and Policy Count "1"
        And I enter the "HnC" Insured information
        # And I do Junk stuff in HNC
        And I enter details in Risk Address Tab
        And I enter details in CoveragesandPremium
        And I issue the Policy
        And I create a new billing account with "Monthly"
        #values include:Monthly,Weekly
        And I Create a Payment Method of "DirectDebit"
        #Payment method values: CreditCard,PCI,DirectDebit
        And I enable "both" Payment Options
        #payment options include: Policy,Billing,both
        And I choose the Paymentplan to "Annual"
        #payment plan eligible values: SixMonthly,Quarterly,Monthly,Annual
        Then I Purchase the policy by clicking on finish

    Scenario: Create a New House Policy for a Customer with Monthly, CreditCard, Annual, Billing details
        Given I create the "HnC" Policy for the Customer
        When I enter a "HnC" , "House" Policy information and Policy Count "1"
        And I enter the "HnC" Insured information
        # And I do Junk stuff in HNC
        And I enter details in Risk Address Tab
        And I enter details in CoveragesandPremium
        And I issue the Policy
        And I create a new billing account with "Monthly"
        #values include:Monthly,Weekly
        And I Create a Payment Method of "CreditCard"
        #Payment method values: CreditCard,PCI,DirectDebit
        And I enable "both" Payment Options
        #payment options include: Policy,Billing,both
        And I choose the Paymentplan to "Annual"
        #payment plan eligible values: SixMonthly,Quarterly,Monthly,Annual
        Then I Purchase the policy by clicking on finish
    ##############################
    Scenario: Create a New House Policy for a Customer with Monthly, DD, SixMonthly, Billing details
        Given I create the "HnC" Policy for the Customer
        When I enter a "HnC" , "House" Policy information and Policy Count "1"
        And I enter the "HnC" Insured information
        # And I do Junk stuff in HNC
        And I enter details in Risk Address Tab
        And I enter details in CoveragesandPremium
        And I issue the Policy
        And I create a new billing account with "Monthly"
        #values include:Monthly,Weekly
        And I Create a Payment Method of "DirectDebit"
        #Payment method values: CreditCard,PCI,DirectDebit
        And I enable "both" Payment Options
        #payment options include: Policy,Billing,both
        And I choose the Paymentplan to "SixMonthly"
        #payment plan eligible values: SixMonthly,Quarterly,Monthly,Annual
        Then I Purchase the policy by clicking on finish

    Scenario: Create a New House Policy for a Customer with Monthly, CreditCard, SixMonthly, Billing details
        Given I create the "HnC" Policy for the Customer
        When I enter a "HnC" , "House" Policy information and Policy Count "1"
        And I enter the "HnC" Insured information
        # And I do Junk stuff in HNC
        And I enter details in Risk Address Tab
        And I enter details in CoveragesandPremium
        And I issue the Policy
        And I create a new billing account with "Monthly"
        #values include:Monthly,Weekly
        And I Create a Payment Method of "CreditCard"
        #Payment method values: CreditCard,PCI,DirectDebit
        And I enable "both" Payment Options
        #payment options include: Policy,Billing,both
        And I choose the Paymentplan to "SixMonthly"
        #payment plan eligible values: SixMonthly,Quarterly,Monthly,Annual
        Then I Purchase the policy by clicking on finish

    Scenario: Create a New House Policy for a with Monthly, DD, Quarterly, Billing details
        Given I create the "HnC" Policy for the Customer
        When I enter a "HnC" , "House" Policy information and Policy Count "1"
        And I enter the "HnC" Insured information
        # And I do Junk stuff in HNC
        And I enter details in Risk Address Tab
        And I enter details in CoveragesandPremium
        And I issue the Policy
        And I create a new billing account with "Monthly"
        #values include:Monthly,Weekly
        And I Create a Payment Method of "DirectDebit"
        #Payment method values: CreditCard,PCI,DirectDebit
        And I enable "both" Payment Options
        #payment options include: Policy,Billing,both
        And I choose the Paymentplan to "Quarterly"
        #payment plan eligible values: SixMonthly,Quarterly,Monthly,Annual
        Then I Purchase the policy by clicking on finish

    Scenario: Create a New House Policy for a Customer with Monthly, CreditCard, Quarterly, Billing details
        Given I create the "HnC" Policy for the Customer
        When I enter a "HnC" , "House" Policy information and Policy Count "1"
        And I enter the "HnC" Insured information
        # And I do Junk stuff in HNC
        And I enter details in Risk Address Tab
        And I enter details in CoveragesandPremium
        And I issue the Policy
        And I create a new billing account with "Monthly"
        #values include:Monthly,Weekly
        And I Create a Payment Method of "CreditCard"
        #Payment method values: CreditCard,PCI,DirectDebit
        And I enable "both" Payment Options
        #payment options include: Policy,Billing,both
        And I choose the Paymentplan to "Quarterly"
        #payment plan eligible values: SixMonthly,Quarterly,Monthly,Annual
        Then I Purchase the policy by clicking on finish

    Scenario: Create a New House Policy for a Customer with Monthly, DD, Monthly, Billing details
        Given I create the "HnC" Policy for the Customer
        When I enter a "HnC" , "House" Policy information and Policy Count "1"
        And I enter the "HnC" Insured information
        # And I do Junk stuff in HNC
        And I enter details in Risk Address Tab
        And I enter details in CoveragesandPremium
        And I issue the Policy
        And I create a new billing account with "Monthly"
        #values include:Monthly,Weekly
        And I Create a Payment Method of "DirectDebit"
        #Payment method values: CreditCard,PCI,DirectDebit
        And I enable "both" Payment Options
        #payment options include: Policy,Billing,both
        And I choose the Paymentplan to "Monthly"
        #payment plan eligible values: SixMonthly,Quarterly,Monthly,Annual
        Then I Purchase the policy by clicking on finish

    Scenario: Create a New House Policy for a Customer with Monthly, CreditCard, Monthly, Billing details
        Given I create the "HnC" Policy for the Customer
        When I enter a "HnC" , "House" Policy information and Policy Count "1"
        And I enter the "HnC" Insured information
        # And I do Junk stuff in HNC
        And I enter details in Risk Address Tab
        And I enter details in CoveragesandPremium
        And I issue the Policy
        And I create a new billing account with "Monthly"
        #values include:Monthly,Weekly
        And I Create a Payment Method of "CreditCard"
        #Payment method values: CreditCard,PCI,DirectDebit
        And I enable "both" Payment Options
        #payment options include: Policy,Billing,both
        And I choose the Paymentplan to "Monthly"
        #payment plan eligible values: SixMonthly,Quarterly,Monthly,Annual
        Then I Purchase the policy by clicking on finish