import { IComponent } from "./BaseComponent";

class Button implements IComponent {
    private button: WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>>;
    setValue(selectors: any, value: string | number) {
        const buttonSelector = `.//*[@${selectors.elementSelector}]/button[contains(text(),"${value}")]`;
        browser.waitForExist(buttonSelector)
        this.button = browser.$(buttonSelector);
        browser.waitForScroll(buttonSelector);
        this.button.click();
    }
}

export { Button }