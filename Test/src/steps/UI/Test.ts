import { expect } from 'chai';
import { Given, When, Then } from 'cucumber';
import EISLoginPage from '../../pages/EISLoginPage';
import EISHomePage from '../../pages/EISHomePage';
import eisQuotePage from '../../pages/EISQuotePage';
import MotorPage from '../../pages/EISMotorPage';
import BillingPage from '../../pages/BillingPage';
import { CustomerData } from '../../common/CustomerData';
import { SharedData } from '../../common/CommonData';
//import { EISLogin } from '../../steps/UI/EISLogin';

var environment = CustomerData.Environment.Tac;
const page = new EISLoginPage(environment);
const homePage = new EISHomePage(environment);
const quotePage = new eisQuotePage(environment);
const motorPage = new MotorPage(environment);
const billingPage = new BillingPage(environment);


Given('I do blah blah', (): void => {
    console.log(SharedData.lastCC4N0);
    browser.url('https://www.google.co.nz/');
});