export enum Response {
    success = 200,
    BadRequest = 400,
    NotFound = 404,
    ServerError = 500,
    GreenTick = "done",
    RedCross = "clear",
}