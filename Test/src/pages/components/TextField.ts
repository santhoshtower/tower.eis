import { IComponent } from "./BaseComponent";

class TextField implements IComponent {
    private textField;
    private submitButton;

    setValue(selectors: any, value: string | number) {

        browser.waitForExist(selectors.elementSelector);
        this.textField = browser.$(selectors.elementSelector);
        this.textField.setValue(value);

        // submitting form
        if (selectors.actionSelector) {
            browser.waitForExist(selectors.actionSelector);
            this.submitButton = browser.$(selectors.actionSelector);
            this.submitButton.click();
        }

        // waiting for result to show after submitting
        if (selectors.resultSelector) {
            browser.waitForExist(selectors.resultSelector)
        }
    }
}

export { TextField }
