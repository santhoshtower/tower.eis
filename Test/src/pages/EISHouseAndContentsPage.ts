import BasePage from './BasePage';
import EISHNCSelector from '../selectors/EISHouseNContnetsSelectors';

export default class EISHouseAndContentsPage extends BasePage {

    get dpdwnSource(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.dpdwnPolicyType);
    }
    get dpdwnInsuredPartySelection(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.dpdwnInsuredPartySelection);
    }
    get rdbtnInsuranceRefusedCancelled(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.rdbtnInsuranceRefusedCancelled);
    }
    get rdbtnInsuranceRefusedCancelled_Yes(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.rdbtnInsuranceRefusedCancelled_Yes);
    }
    get dpdwnInsuranceRefused(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.dpdwnInsuranceRefused);
    }
    get rdbtnLossDamaged(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.rdbtnLossDamaged);
    }
    get rdbtnLossDamagedYes(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.rdbtnLossDamagedYes);
    }
    get dpdwnLossType(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.dpdwnLossType);
    }
    get txtBoxLossYear(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.txtBoxLossYear);
    }
    get rdbtnConvictions(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.rdbtnConvictions);
    }
    get rdbtnConvictions_Yes(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.rdbtnConvictions_Yes);
    }
    get dpdwnConvictionType(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.dpdwnConvictionType);
    }
    get txtBoxConvictionYear(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.txtBoxConvictionYear);
    }
    get rdbtnBankRuptcy(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.rdbtnBankRuptcy);
    }
    get rdbtnBankRuptcy_Yes(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.rdbtnBankRuptcy_Yes);
    }
    get dpdwnBankruptcy(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.dpdwnBankruptcy);
    }
    get txtBoxBankruptcyYear(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.txtBoxBankruptcyYear);
    }
    get dpdwnDwellingType(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.dpdwnDwellingType);
    }
    get rdbtnBodyCorporate(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.rdbtnBodyCorporate_Yes);
    }
    get rdbtnBodyCorporate_No(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.rdbtnBodyCorporate_No);
    }
    get dpdwnStoreis(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.dpdwnStoreis);
    }
    get dpdwnUnits(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.dpdwnUnits);
    }
    get txtBxYearBuilt(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.txtBxYearBuilt);
    }
    get dpdwnConstructionType(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.dpdwnConstructionType);
    }
    get dpdwnRoofType(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.dpdwnRoofType);
    }
    get txtBxFloorArea(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.txtBxFloorArea);
    }
    get rdbtnExternalGarage(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.rdbtnExternalGarage);
    }
    get txtbxSumInsured(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.txtbxSumInsured);
    }
    get rdbtnWaterTight(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.rdbtnWaterTight);
    }
    get rdbtnHazard(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.rdbtnHazard);
    }
    get dpdwnOccupancyType(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.dpdwnOccupancyType);
    }
    get rdbtnAirBnB(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.rdbtnAirBnB);
    }

    // junk stuff
    get InsureAddress(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.InsureAddress);
    }
    get SearchAddressInsured(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.SearchAddressInsured);
    }
    get SelectAddress(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.SelectAddress);
    }
    get dpdwnState(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.dpdwnState);
    }
    get ValidateBtn(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.ValidateBtn);
    }

    // Coverages & Premium
    get tabCnP(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.tabCnP);
    }
    get dpdwnPaymentPlan(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.dpdwnPaymentPlan);
    }
    get dpdwnPolicyExcess(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.dpdwnPolicyExcess);
    }
    get txtBxDwellingLimit(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.txtBxDwellingLimit);
    }
    get txtBxPersonalProperty(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.txtBxPersonalProperty);
    }
    get txtBxLossofUse(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.txtBxLossofUse);
    }
    get txtBxFairRentalValue(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.txtBxFairRentalValue);
    }
    get txtBxAdditionalLivingExp(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.txtBxAdditionalLivingExp);
    }
    get btnCalculatePremium(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.btnCalculatePremium);
    }
    get tabDocument(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.tabDocument);
    }
    get suppressDocument(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.suppressDocument);
    }
    get btnSaveNext(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.btnSaveNext);
    }

    // issue the policy
    get dpdwnTakeAction(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> {
        return browser.element(EISHNCSelector.dpdwnTakeAction);
    }
}
