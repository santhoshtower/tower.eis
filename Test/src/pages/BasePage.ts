export default class BasePage {
    protected url: string;

    constructor(url: string) {
        this.url = url;
    }

    get pauseTime(): number {
        return 1000;
    }

    get currentPageUrl(): string {
        return browser.getUrl();
    }

    public open(url: string = this.url): void {
        browser.url(url);
    }

    public pause(milliseconds: number = 1000): void {
        browser.pause(milliseconds);
    }

    public waitForPageLoad(): void {
        browser.waitForVisible('//*[@id="content"]/div', 30000)
    }

    public waitForExist(selector: string): boolean {
        return browser.waitForExist(selector, 20000)
    }

    public allItemsVisible(items: Array<WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>>>): boolean {
        let allListItemsVisible = true;
        items.forEach(function (item) {
            if (!item.isVisible()) {
                allListItemsVisible = false;
                return;
            }
        });

        return allListItemsVisible;
    }
}
