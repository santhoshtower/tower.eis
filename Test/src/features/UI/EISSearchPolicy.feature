@ui
Feature: Search for a policy and Open

    Scenario: Search for a Policy or Quote
        Given I Login into the EIS SIT
        And I search for "Policy" with string "%"

    Scenario: Search for a Customer by FirstName
        Given I Login into the EIS SIT
        And I search for "Customer" with string "Santhosh Ippili Tester"