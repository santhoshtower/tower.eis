
const eisSearchPlusPageSelector = {

    btnSearchPlus: '//*[@id="topQuickSearchForm:searchExtendedBtn"]',
    tabPolicy: '//*[@id="searchForm:entityTypeSelect"]/tbody/tr/td[4]',
    txtBxPolicynQuote: '//*[@id="searchForm:searchFormME_policyNumber"]',
    txtBxFirstName: '//*[@id="searchForm:searchFormME_firstName"]',
    btnSearch: '//*[@id="searchForm:searchBtn"]',
    linkfirstPolicy: '//*[@id="searchTable1Form:body_searchTable1:0:selectPolicy"]',

}

export default eisSearchPlusPageSelector;