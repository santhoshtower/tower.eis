const brand = process.env.brand || 'tower';
const domian = "http://azs4siteisapp01:8080/twr-app/login";
// const domian = "http://eis-ts01.towercloud.co.nz:8080/twr-app/login";
// const domian = "http://eis-sit.towercloud.co.nz:8080/twr-app/login";
module.exports = {
    CAR_QUOTE_PAGE: `${domian}`,
    API: `https://api-tac.towercloud.co.nz/`,
};