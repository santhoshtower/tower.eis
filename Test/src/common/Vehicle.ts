class Vehicle {
    public rego;
    public make;
    public model;
    public year;
    public style;
    public type;
    public distance;


    constructor(rego, make, model, year, style, type, distance) {
        this.rego = rego;
        this.make = make;
        this.model = model;
        this.year = year;
        this.style = style;
        this.type = type;
        this.distance = distance;
    }
}
export { Vehicle }