const TemplateCustomer = {
    createCustomer: {
        customerNumber: "string",//Required must be an existing customer number (its not going to be the id for the new entry)
        customerType: "string",//Required 
        accountNumber: "string",
        customerStatus: "string",//Required enum {UNQUALIFIED,IN_QUALIFICATION, INVALID, QUALIFIED, CUSTOMER}
        sourceCd: "string",//Required
        ratingCd: "string",//Required 
        displayValue: "string",
        brandCd: "string",
        preferredSpokenLanguageCd: "string",
        preferredWrittenLanguageCd: "string",
        paperless: "string",
        segments: [
            "string",
            "string"
        ],
        registeredOnline: "string",
        archived: "string",
        sourceOfExchangeRate: "string",
        rateDateIntake: "string",
        rateDatePayment: "string",
        preferredCurrency: "string",
        individualDetails: {
            firstName: "string",//Required
            lastName: "string",//Required
            middleName: "string",
            title: "string",
            suffix: "string",
            nickname: "string",
            birthDate: new Date(),
            genderCd: "string",
            maritalStatusCd: "string",
            taxId: "string",
            deceased: "string",
            deathDate: new Date(),
            deathNotificationReceived: "string",
            occupationCd: "string",
            occupationDescription: "string",
            tobaccoCd: "string",
            citizenshipCd: "string",
            interests: [
                "string"
            ],
            disabilities: [
                "string"
            ],
            designationCd: "string",
            designationDescription: "string",
            extensionFields: {
                propertyName: "string"
            },
            associateEmployments: "string",
            associateProviders: "string",
            associateBusinessEntity: "string"
        },
        businessDetails: {
            legalName: "string",
            businessType: "string",
            dbaName: "string",
            sicCode: "string",
            naicsCode: "string",
            legalId: "string",
            dateStarted: new Date(),
            taxExemptInd: false,
            groupSponsorInd: false,
            numberOfContinuous: 123456,
            numberOfEmployees: 123456,
            entityType: [
                "string"
            ],
            useAsReference: false,
            referenceCategories: [
                "string"
            ],
            referenceComment: "string",
            extensionFields: {
                propertyName: "string"
            }
        },
        extensionFields: {
            propertyName: "string"
        },
        preferredContactMethod: "EMAIL",
        addresses: {
            id: 123456,
            contactMethod: "ADDRESS",
            contactType: "",
            preferredInd: false,
            doNotSolicitInd: false,
            comment: "",
            communicationPreferences: ["", ""],
            extensionFields: { propertyName: "propertyValue" },
            temporary: false,
            effectiveFrom: new Date(),
            effectiveTo: new Date(),
            duration: 123456,
            stateProvCd: "",
            postalCode: "",
            countryCd: "",
            addressLine1: "",
            addressLine2: "",
            addressLine3: "",
            city: "",
            county: "",
            inCareOf: "",
            subdivision: "",
            attention: "",
            validationIndicator: false,
            latitude: 123456,
            longitude: 123456,
            accuracy: "",
            referenceId: "",
        },
        chats: {
            id: 123456,
            contactMethod: "CHAT",
            contactType: "",
            preferredInd: false,
            doNotSolicitInd: false,
            comment: "",
            communicationPreferences: ["", ""],
            extensionFields: { propertyName: "propertyValue" },
            chatId: "",
        },
        emails: {
            id: 123456,
            contactMethod: "EMAIL",
            contactType: "",
            preferredInd: false,
            doNotSolicitInd: false,
            comment: "",
            communicationPreferences: ["", ""],
            extensionFields: { propertyName: "propertyValue" },
            emailId: "",
            consentStatus: "",
            consentDate: new Date(),
        },
        phones: {
            id: 123456,
            contactMethod: "PHONE",
            contactType: "",
            preferredInd: false,
            doNotSolicitInd: false,
            comment: "",
            communicationPreferences: ["", ""],
            extensionFields: { propertyName: "propertyValue" },
            temporary: false,
            effectiveFrom: new Date(),
            effectiveTo: new Date(),
            duration: 123456,
            phoneNumber: "",
            consentToTextStatus: "",
            consentToTextDate: new Date(),
            preferredDaysToContact: ["", ""],
            preferredTimesToContact: ["", ""],
            phoneExtension: "",
            consentStatus: "",
            consentStatusDeniedReason: "",
            consentToTextStatusDeniedReason: "",
            consentDate: new Date(),
        },
        socialNets: {
            id: 123456,
            contactMethod: "SOCIAL_NET",
            contactType: "",
            preferredInd: false,
            doNotSolicitInd: false,
            comment: "",
            communicationPreferences: ["", ""],
            extensionFields: { propertyName: "propertyValue" },
            socialNetId: "",
        },
        webAddresses: {
            id: 123456,
            contactMethod: "WEB_ADDRESS",
            contactType: "",
            preferredInd: false,
            doNotSolicitInd: false,
            comment: "",
            communicationPreferences: ["", ""],
            extensionFields: { propertyName: "propertyValue" },
            webAddress: "",
        },
        agencies: {
            agencyCode: "",
        },
        indCustomerAdditionalNames: {
            id: 123456,
            salutation: "",
            firstName: "",
            middleName: "",
            lastName: "",
            suffix: "",
            designationCd: "",
            designationDescription: "",
        },
        businessCustomerAdditionalNames: {
            id: 123456,
            nameDba: "",
        },
        customerEmployments: {
            id: 123456,
            employerName: "",
            occupationCd: "",
            occupationDescription: "",
            occupationStatusCd: "",
            jobTitleCd: "",
            jobTitleDescription: "",
            asOfDate: new Date(),
        },
        providers: {
            id: 123456,
            providerId: "",
            practiceId: "",
            facilityId: "",
            effectiveDate: new Date(),
            expirationDate: new Date(),
            comment: "",
            extensionFields: { propertyName: "propertyValue" },
        },
        genericRelationships: {
            id: 123456,
            relationshipCustomerNumber: "",
            relationshipRole: "",
            relationshipDescription: "",
            extensionFields: { propertyName: "propertyValue" },
        },
        navigationLinks: {
            id: 123456,
            sourceNumber: "",
            targetNumber: "",
            linkType: "",
        }
    },
    quotes: [{
        customerNumber: "510026",
        effectiveDate: "2018-10-25T09:42:10Z",
        inceptionDate: "2018-09-12T08:00:00Z",
        expirationDate: "2018-11-01T01:48:44Z",
        sourceCd: "NEW",
        declarationStatusCd: "ANSW",
        typeOfPolicyCd: "PMT",
        rateEffectiveDate: "2018-09-11T08:00:00Z",
        rateEffectiveDateOverridenInd: false,
        producerCd: "Tower",
        subProducerCd: "Tower",
        brandCd: "Tower",
        policyCount: 1,
        campaignCode: "CAMPAIGN_CODE",
        externalCustomerNo: "1223",
        insuredPrivacyDeclarationAgreedInd: true,
        insuredCustomerDisclosureDeclarationAgreedInd: true,
        insured48HourStandDownInformedInd: true,
        paymentPlanCd: "ANNUAL",
        packageCd: "Comp_Agreed",
        insuredsAndDrivers: [
            {
                individual: {
                    firstName: "John",
                    lastName: "Smith",
                    dateOfBirth: "1984-09-28",
                    genderCd: "male"
                },
                phones: [
                    {
                        phone: "006412345678",
                        phoneCountryCd: "NZ"
                    }
                ],
                emails: [
                    {
                        email: "john.smith@email.com"
                    }
                ],
                insured: {
                    primaryInsuredInd: true,
                    claimsDeclinedInd: false,
                    convictionsInd: true,
                    convictions: [
                        {
                            convictionTypeCd: "BURGL",
                            convictionYear: 2004
                        }
                    ],
                    carInsuranceDeclinedInd: true,
                    bankruptedInd: true
                },
                driver: {
                    driverTypeCd: "MD",
                    lossDamageInd: true,
                    lossDamages: [
                        {
                            lossDamageTypeCd: "COLEP",
                            lossDamageYear: 2017,
                            driverLossExcessPaidInd: true
                        }
                    ],
                    offenceInd: true,
                    offences: [
                        {
                            offenceTypeCd: "CLSDRV",
                            offenceYear: 2016
                        }
                    ],
                    licenses: [
                        {
                            howLongHeldYourDriverLicenseCd: "001",
                            licenseWasSuspendedInd: false
                        }
                    ]
                }
            }
        ],
        vehicles: [
            {
                marketValue: 1000,
                agreedValueMin: 1000,
                agreedValueMax: 3000,
                adjustedMarketValue: 2000,
                costNew: 5000,
                vehicleUsageCd: "TRDPRS",
                registrationNo: "123456",
                make: "AUDI",
                model: "100",
                year: "1993",
                bodyStyle: "Sedan",
                type: "E Sedan 4dr Auto 4sp 2.6i",
                otherModelDescription: "other model description",
                alarmInd: true,
                useStatusCd: "5N7",
                registeredAndWarrantedInd: true,
                declaredAnnualKilometresCd: "15000",
                useForBusinessInd: false,
                ownVehicleInd: true,
                parkingLocationCd: "GARAGE",
                parkingSameAsInsuredAddressInd: true,
                parkingAddress: {
                    city: "Wellington",
                    stateProvCd: "WLG",
                    postalCode: "6011",
                    countryCd: "NZ",
                    addressLine1: "Courtney Place",
                    addressLine2: "11",
                    addressLine3: "2N",
                    referenceId: "0",
                    addressValidatedInd: true
                },
                hotListFlags: {
                    declineInd: false,
                    referInd: false,
                    driverU21Ind: false,
                    driverU25Ind: false,
                    vehicleExcessInd: false,
                    theftExcessInd: false,
                    alarmInd: true,
                    windscreenInd: false
                },
                excessAmount: 1000,
                coverages: {
                    vehicleScheduledItemsCoverage: {
                        limitAmount: 2000,
                        limitAmountDisplay: "2000"
                    },
                    vehicleRoadWiseBenefitCoverage: {
                        limitAmount: 1000
                    },
                    vehicleTrailersCoverage: {
                        limitAmount: 3000
                    },
                    vehicleU25DriverExclusionBenefitCoverage: {
                        limitAmount: 4000
                    },
                    vehicleWindscreenAndWindowGlassExcessBuyOutCoverage: {
                        limitAmount: 5000
                    }
                }
            }
        ]
    }],
    policies: [{

    }],
    claims: [{

    }
    ],
}

export { TemplateCustomer };