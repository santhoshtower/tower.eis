declare namespace WebdriverIO {
    interface Client<T> {
        waitForScroll(selector: string): void;
        waitForSpinner;
    }
}

browser.addCommand('waitForScroll', (selector) => {
    browser.waitUntil(() => {
        if (!browser.isVisibleWithinViewport(selector)) {
            browser.$(selector).scroll();
        }
        return browser.isVisibleWithinViewport(selector)
    });
});

browser.addCommand('clickAndWait', (element) => {
    browser.waitUntil(() => {
        element.click();
        browser.waitForSpinner();
        return true;
    });
});

browser.addCommand('waitForSpinner', (timeout) => {
    const spinner = '//*[@id="ajaxLoadingModalBox_container"]';
    var max = 50;
    if (timeout != null) {
        //console.log("Timeout:", timeout);
        max = timeout / 200;
    }
    console.log("wait for spinner");
    browser.waitForExist(spinner, 1000);
    //console.log("Spinner exists");
    //console.log("Max:", max);
    var count = 0;
    while (count < max) {
        //console.log("Spinner loop entered");
        count++;
        if (browser.isVisibleWithinViewport(spinner)) {
            console.log("Spinner is visible");
            browser.pause(200);
            //console.log("Spinner still here waiting 200");
        } else {
            browser.pause(1000);
            if (browser.isVisibleWithinViewport(spinner)) {
                console.log("Spinner came back");
                //console.log("modal box came back!");
            } else {
                console.log("Spinner is gone");
                break;
            }
        }
    }
    if (count === max) {
        console.log("max loops reached waiting for spinner");
    }
    // else if (browser.isVisibleWithinViewport(spinner)) {
    //     //console.log("modal box came back!");
    // }
    //console.log("Spinner gone");
    //browser.waitForExist('//*[@id="ajaxLoadingModalBox_container"]', 5000, true);
});
