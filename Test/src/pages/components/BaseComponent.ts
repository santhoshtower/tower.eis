import { TextField } from "./TextField";
import { Button } from "./Button";
import { DropdownField } from "./DropdownField";

export interface IComponent {
    setValue(selectors: object, value: string | number);
}

export class ComponentFactory {
    createComponent(type: string): IComponent {
        try {
            switch (type) {
                case 'button': {
                    return new Button();
                }
                case 'textField': {
                    return new TextField();
                }
                case 'dropdownField': {
                    return new DropdownField();
                }
                default:
                    throw new Error('No Component found');
            }
        }
        catch (e) {
            console.log(`Component type: ${type}`)
            console.log(e);
        }
    }
}
