import { Given, When, Then, setDefaultTimeout } from 'cucumber';
import { BasicCustomer } from '../../data/CustomerDataCreation/Customers/BasicCustomer';
//import { AnotherCustomer } from '../../data/CustomerDataCreation/Customers/AnotherCustomer';
import { CustomerDataService } from '../../data/CustomerDataCreation/CustomerDataService';

var createCustomerResult;

When('I create customers based on json data', { timeout: 60 * 1000 }, async function () {
  setDefaultTimeout(60 * 1000);
  CustomerDataService.createCustomer(BasicCustomer);
  //CustomerDataService.createCustomer(AnotherCustomer);

  // for (var i = 0; i < Customers.length; i++) {
  //   console.log("Perform request iteration: ", i);
  //   createCustomerResult = await CustomerDataService.createCustomer(Customers[i], customersService);
  //   console.log(`create customer result ${i}`, createCustomerResult);
  //   //console.log("create customer result take 2 ", CustomerDataService.getLastCustomer());
  // }
});

// function getMinimalQuoteBody() {
//   const params = <PrecAuQuoteCreateRequest>{
//     "customerNumber": "510026",
//     "expirationDate": "2018-11-01T01:48:44Z",
//     "typeOfPolicyCd": "PMT",
//     "brandCd": "Tower",
//     "insuredPrivacyDeclarationAgreedInd": true,
//     "insuredCustomerDisclosureDeclarationAgreedInd": true,
//     "insured48HourStandDownInformedInd": true,
//     "paymentPlanCd": "ANNUAL",
//     "packageCd": "Comp_Agreed",
//     "policyCount": 1,
//     "insuredsAndDrivers": [
//       {
//         "individual": {
//           "firstName": "John",
//           "lastName": "Smith",
//           "dateOfBirth": "1984-09-28",
//           "genderCd": "male"
//         },
//         "phones": [
//           {
//             "phone": "006412345678",
//             "phoneCountryCd": "NZ"
//           }
//         ],
//         "emails": [
//           {
//             "email": "john.smith@email.com"
//           }
//         ],
//         "insured": {
//           "primaryInsuredInd": true,
//           "claimsDeclinedInd": false,
//           "convictionsInd": true,
//           "convictions": [
//             {
//               "convictionTypeCd": "BURGL",
//               "convictionYear": 2004
//             }
//           ],
//           "carInsuranceDeclinedInd": true,
//           "bankruptedInd": true
//         },
//         "driver": {
//           "driverTypeCd": "MD",
//           "lossDamageInd": true,
//           "lossDamages": [
//             {
//               "lossDamageTypeCd": "COLEP",
//               "lossDamageYear": 2017,
//               "driverLossExcessPaidInd": true
//             }
//           ],
//           "offenceInd": true,
//           "offences": [
//             {
//               "offenceTypeCd": "CLSDRV",
//               "offenceYear": 2016
//             }
//           ],
//           "licenses": [
//             {
//               "howLongHeldYourDriverLicenseCd": "001",
//               "licenseWasSuspendedInd": false
//             }
//           ]
//         }
//       }
//     ],
//     "vehicles": [
//       {
//         "marketValue": 1000,
//         "agreedValueMin": 1000,
//         "agreedValueMax": 3000,
//         "adjustedMarketValue": 2000,
//         "costNew": 5000,
//         "vehicleUsageCd": "TRDPRS",
//         "registrationNo": "123456",
//         "make": "AUDI",
//         "model": "100",
//         "year": "1993",
//         "bodyStyle": "Sedan",
//         "type": "E Sedan 4dr Auto 4sp 2.6i",
//         "otherModelDescription": "other model description",
//         "alarmInd": true,
//         "useStatusCd": "5N7",
//         "registeredAndWarrantedInd": true,
//         "declaredAnnualKilometresCd": "15000",
//         "useForBusinessInd": false,
//         "ownVehicleInd": true,
//         "parkingLocationCd": "GARAGE",
//         "parkingSameAsInsuredAddressInd": true,
//         "parkingAddress": {
//           "city": "Wellington",
//           "stateProvCd": "WLG",
//           "postalCode": "6011",
//           "countryCd": "NZ",
//           "addressLine1": "Courtney Place",
//           "addressLine2": "11",
//           "addressLine3": "2N",
//           "referenceId": "0",
//           "addressValidatedInd": true
//         },
//         "hotListFlags": {
//           "declineInd": false,
//           "referInd": false,
//           "driverU21Ind": false,
//           "driverU25Ind": false,
//           "vehicleExcessInd": false,
//           "theftExcessInd": false,
//           "alarmInd": true,
//           "windscreenInd": false
//         },
//         "excessAmount": 1000,
//         "coverages": {
//           "vehicleScheduledItemsCoverage": {
//             "limitAmount": 2000,
//             "limitAmountDisplay": "2000"
//           },
//           "vehicleRoadWiseBenefitCoverage": {
//             "limitAmount": 1000
//           },
//           "vehicleTrailersCoverage": {
//             "limitAmount": 3000
//           },
//           "vehicleU25DriverExclusionBenefitCoverage": {
//             "limitAmount": 4000
//           },
//           "vehicleWindscreenAndWindowGlassExcessBuyOutCoverage": {
//             "limitAmount": 5000
//           }
//         }
//       }
//     ]
//   }

//   return params;
// }

