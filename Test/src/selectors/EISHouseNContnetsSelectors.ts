
const eisHousenContentsSelector = {
    //Policy Tab
    dpdwnPolicyType: '//*[@id="policyDataGatherForm:sedit_Policy_typeOfPolicyCd"]',
    // Insured tab
    dpdwnInsuredPartySelection: '//*[@id="policyDataGatherForm:sedit_PreconfigInsured_partySelection"]',
    rdbtnInsuranceRefusedCancelled: '//*[@id="policyDataGatherForm:sedit_InsuredInsuranceDeclined_yesNoAnswer:1"]',
    rdbtnInsuranceRefusedCancelled_Yes: '//*[@id="policyDataGatherForm:sedit_InsuredInsuranceDeclined_yesNoAnswer:0"]',
    dpdwnInsuranceRefused: '//*[@id="policyDataGatherForm:sedit_InsuredInsuranceDeclined_otherAnswer"]',
    rdbtnLossDamaged: '//*[@id="policyDataGatherForm:sedit_InsuredHouseLossDamageQuestion_yesNoAnswer:1"]',
    rdbtnLossDamagedYes: '//*[@id="policyDataGatherForm:sedit_InsuredHouseLossDamageQuestion_yesNoAnswer:0"]',
    dpdwnLossType: '//*[@id="policyDataGatherForm:sedit_InsuredHouseLossDamage_lossDamageType"]',
    txtBoxLossYear: '//*[@id="policyDataGatherForm:sedit_InsuredHouseLossDamage_lossDamageYear"]',
    rdbtnConvictions: '//*[@id="policyDataGatherForm:sedit_InsuredConvictionQuestion_convictionInd:1"]',
    rdbtnConvictions_Yes: '//*[@id="policyDataGatherForm:sedit_InsuredConvictionQuestion_convictionInd:0"]',
    dpdwnConvictionType: '//*[@id="policyDataGatherForm:sedit_InsuredConvictions_typeCd"]',
    txtBoxConvictionYear: '//*[@id="policyDataGatherForm:sedit_InsuredConvictions_year"]',
    rdbtnBankRuptcy: '//*[@id="policyDataGatherForm:sedit_InsuredBankruptcyNoAssetProcedure_yesNoAnswer:1"]',
    rdbtnBankRuptcy_Yes: '//*[@id="policyDataGatherForm:sedit_InsuredBankruptcyNoAssetProcedure_yesNoAnswer:0"]',
    dpdwnBankruptcy: '//*[@id="policyDataGatherForm:sedit_InsuredBankruptcyNoAssetProcedure_otherAnswer"]',
    txtBoxBankruptcyYear: '//*[@id="policyDataGatherForm:sedit_InsuredBankruptcyNoAssetProcedure_integerAnswer"]',
    //Risk Address
    dpdwnDwellingType: '//*[@id="policyDataGatherForm:sedit_DwellingTypeQuestionAnswer_otherAnswer"]',
    rdbtnBodyCorporate_Yes: '//*[@id="policyDataGatherForm:sedit_HousePartOfBodyCorpQuestionAnswer_yesNoAnswer:0"]',
    rdbtnBodyCorporate_No: '//*[@id="policyDataGatherForm:sedit_HousePartOfBodyCorpQuestionAnswer_yesNoAnswer:1"]',
    dpdwnStoreis: '//*[@id="policyDataGatherForm:sedit_PreconfigConstructionInfo_numberOfStories"]',
    dpdwnUnits: '//*[@id="policyDataGatherForm:sedit_PreconfigConstructionInfo_numberOfSelfContainedUnits"]',
    txtBxYearBuilt: '//*[@id="policyDataGatherForm:sedit_PreconfigConstructionInfo_yearBuilt"]',
    dpdwnConstructionType: '//*[@id="policyDataGatherForm:sedit_PreconfigConstructionInfo_constructionTypeCd"]',
    txtBxFloorArea: '//*[@id="policyDataGatherForm:sedit_PreconfigConstructionInfo_livingArea"]',
    dpdwnRoofType: '//*[@id="policyDataGatherForm:sedit_PreconfigConstructionInfo_roofType"]',
    rdbtnExternalGarage: '//*[@id="policyDataGatherForm:sedit_ExternalGarageOrOutbuildingQuestionAnswer_yesNoAnswer:1"]',
    txtbxSumInsured: '//*[@id="policyDataGatherForm:sedit_RiskItemAddressSumInsured_sumInsured"]',
    rdbtnWaterTight: '//*[@id="policyDataGatherForm:sedit_PropertySecureMaintainedNoDamageQuestionAnswer_yesNoAnswer:0"]',
    rdbtnHazard: '//*[@id="policyDataGatherForm:sedit_PropertyAtRiskFromNaturalHazardQuestionAnswer_yesNoAnswer:0"]',
    dpdwnOccupancyType: '//*[@id="policyDataGatherForm:sedit_PreconfigOccupancyTypeQuestionAnswer_otherAnswer"]',
    rdbtnAirBnB: '//*[@id="policyDataGatherForm:sedit_PreconfigBusinessConductedQuestionAnswer_yesNoAnswer:1"]',


    //Junk Stuff
    dpdwnState: '//*[@id="policyDataGatherForm:sedit_PreconfigHomeRiskItemAddressContact_address_stateProvCd"]',
    InsureAddress: '//*[@id="policyDataGatherForm:sedit_PreconfigHomeRiskItemAddressContact_sameInsuredAddressInd:1"]',
    SearchAddressInsured: '//*[@id="policyDataGatherForm:PreconfigHomeRiskItemAddressContact_addressQueryStr_input"]',
    SelectAddress: '//*[@id="policyDataGatherForm:PreconfigHomeRiskItemAddressContact_addressQueryStr_panel"]/table/tbody/tr[1]/td',
    ValidateBtn: '//*[@id="policyDataGatherForm:formGrid_PreconfigHomeRiskItemAddressContact-12"]/td[3]/a',


    // Claim Reports Tab
    // Mortgages Tab
    // Coverages and Premium
    tabCnP: '//*[@id="policyDataGatherForm:tabListList_1:5:linkLabel"]',
    dpdwnPaymentPlan: '//*[@id="policyDataGatherForm:sedit_TwrPolicyPaymentPlan_paymentPlanType"]',
    dpdwnPolicyExcess: '//*[@id="policyDataGatherForm:sedit_PolicyExcessAndLargeRiskId_policyExcess"]',
    txtBxDwellingLimit: '//*[@id="policyDataGatherForm:QuoteVariation_PreconfigPreCovA_limitAmount_attributes_PreconfigPreCovA_limitAmount_limitAmount"]',
    txtBxPersonalProperty: '//*[@id="policyDataGatherForm:QuoteVariation_PreconfigPreCovC_limitAmount_attributes_PreconfigPreCovC_limitAmount_limitAmount"]',
    txtBxLossofUse: '//*[@id="policyDataGatherForm:QuoteVariation_PreconfigPreCovD_limitAmount_attributes_PreconfigPreCovD_limitAmount_limitAmount"]',
    txtBxFairRentalValue: '//*[@id="policyDataGatherForm:QuoteVariation_PreconfigPreCovDFair_limitAmount_attributes_PreconfigPreCovDFair_limitAmount_limitAmount"]',
    txtBxAdditionalLivingExp: '//*[@id="policyDataGatherForm:QuoteVariation_PreconfigPreCovEAdd_limitAmount_attributes_PreconfigPreCovEAdd_limitAmount_limitAmount"]',
    btnCalculatePremium: '//*[@id="policyDataGatherForm:actionButton_PreconfigPremiumCalculationAction"]',
    // Scheduled Property
    // Forms
    // Documents
    tabDocument: '//*[@id="policyDataGatherForm:tabListList_1:8:linkLabel"]',
    suppressDocument: '//*[@id="policyDataGatherForm:sedit_PreconfigDocumentsDelivery_suppressDocumentGeneration:1"]',
    //issue the policy
    dpdwnTakeAction: '//*[@id="productContextInfoForm:moveToBox"]',
    btnSaveNext: '//*[@id="policyDataGatherForm:save_footer"]',
}

export default eisHousenContentsSelector;