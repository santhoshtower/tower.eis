import BasePage from './BasePage';

export default class EISSearchPlusPage extends BasePage {

    get btnSearchPlus(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="topQuickSearchForm:searchExtendedBtn"]') };
    get tabPolicy(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="searchForm:entityTypeSelect"]/tbody/tr/td[4]') };
    get txtBxPolicynQuote(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="searchForm:searchFormME_policyNumber"]') };
    get txtBxFirstName(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="searchForm:searchFormME_firstName"]') };
    get btnSearch(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="searchForm:searchBtn"]') };
    get linkfirstPolicy(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="searchTable1Form:body_searchTable1:0:selectPolicy"]') };


    public performSearch(type, searchValue) {
        this.btnSearchPlus.click();
        this.tabPolicy.waitForExist();
        if (type == "Policy") {
            this.tabPolicy.click();
            this.txtBxPolicynQuote.waitForExist();
            this.txtBxPolicynQuote.setValue(searchValue);
            this.btnSearch.click();
            this.linkfirstPolicy.waitForExist();
            this.linkfirstPolicy.click();
        } else if (type == "Customer") {
            this.txtBxFirstName.setValue(searchValue);
            this.btnSearch.click();
        }
    }
}
