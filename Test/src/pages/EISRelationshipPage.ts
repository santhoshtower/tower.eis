import BasePage from './BasePage';
import { Person } from '../common/Person';

export default class EISRelationshipPage extends BasePage {
    get relationshipTab(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:crmMainTabsList:1:link"]') };
    get btnAddRelationship(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:addRelationshipBtn"]') };
    get rdBtnNewRltnInd(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:relationshipType_0:0"]') };
    get rdBtnNewRltnNonInd(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:relationshipType_0:1"]') };
    get dpdwnSalutation(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:generalInfoLeft_0_salutation"]') };
    get txtbxFirstName(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:generalInfoLeft_0_firstName"]') };
    get txtbxLastName(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:generalInfoLeft_0_lastName"]') };
    get dpdwnRelationship(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:generalInfoRight_0_relationshipRole"]') };
    get dpdwnReverseRelationship(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:generalInfoRight_0_reversedRelationshipRole"]') };
    get txtBoxDOB(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:generalInfoRight_0_birthDateInputDate"]') };
    get dpdwnGender(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:generalInfoRight_0_genderCd"]') };
    get btnDone(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:doneBtn_footer"]') };
    get tabContactsnRelationship(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="generalTab:customerSecondTabsList:1:link"]') };
    get rltnNameVerify(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:customerContactInfoTogglePanel:header"]/table/tbody/tr/td[2]/div[1]') };
    get rltnNameVerify1(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:newRelationshipsTogglePanel_0:header"]/table/tbody/tr/td[2]/div[2]') };

    //Currently not using
    get btnYes(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="searchForm:yes"]') };
    get txtbxSearchAddress(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:addressTypeInfo_0_searchAddress_input"]') };
    get selectAddress(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:addressTypeInfo_0_searchAddress_panel"]/ul/li[1]') };
    get btnNext(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="crmForm:nextBtn_footer"]') };
    get lblCustomerName(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="custInfoForm:name"]') };
    get lblAddress(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="custInfoForm:j_id_27_4g_5_1_3:addressLine"]') };


    public selectAddRelationship(): any {
        this.btnAddRelationship.click();
    }

    public getRelationshipNameLabel(): any {
        return this.rltnNameVerify.getText();
    }

    public getCustomerNameLabel(): any {
        return this.rltnNameVerify1.getText();
    }

    public selectTab(): any {
        this.tabContactsnRelationship.click();
        browser.waitForSpinner();
    }

    public addRelationship(person: Person, relationshipType) {
        this.btnAddRelationship.click();

        this.dpdwnSalutation.selectByVisibleText(person.salutation);
        this.txtbxFirstName.setValue(person.firstname);
        this.txtbxLastName.setValue(person.lastname);

        this.dpdwnGender.selectByVisibleText(person.gender);
        this.dpdwnRelationship.selectByVisibleText(relationshipType);
        // this.dpdwnReverseRelationship.selectByVisibleText(CustomerData.RelationshipInd.ReverseRelationship);
        this.txtBoxDOB.setValue(person.dateOfBirth);
        browser.waitForSpinner();
        this.btnDone.click();
        browser.waitForSpinner();
    }
}
