const maxInstances = process.env.MAX_INSTANCES || 5;
module.exports = {
    maxInstances: maxInstances,
    capabilities: [
        {
            browserName: 'chromium-browser',
            acceptSslCerts: 'true',
            trustAllSSLCertificates: 'true',
            chromeOptions: {
                args: ['--headless', '--disable-gpu', '--window-size=1280,800', '-disable-dev-shm-usage', '--no-sandbox'],
            },
            specs: [
                './src/features/UI/*.feature'
            ],
        }
    ],
    cucumberOpts: {
        tagExpression: '(@ui) and not @Pending and not @dev',
    }
};