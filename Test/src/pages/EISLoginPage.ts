import BasePage from './BasePage';

export default class EISLoginPage extends BasePage {
    get username(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="loginForm:j_username"]') };
    get password(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="loginForm:j_password"]') };
    get submitButton(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="loginForm:submitForm"]') };

    public login(username, password) {
        console.log("enter username", username);
        this.username.waitForExist();
        this.username.setValue(username);

        console.log("enter password", password);
        this.password.setValue(password);
        console.log("click submit")
        this.submitButton.click();
        browser.waitForSpinner();
    }
}