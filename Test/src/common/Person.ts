class Person {
    public salutation;
    public firstname;
    public lastname;
    public gender;
    public dateOfBirth;

    constructor(saluation, firstname, lastname, gender, dob) {
        this.salutation = saluation;
        this.firstname = firstname;
        this.lastname = lastname;
        this.gender = gender;
        this.dateOfBirth = dob;
    }
}
export { Person }