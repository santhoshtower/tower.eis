@ui
Feature: Create a New Motor Policy for a New Customer
    QA Engineer should be logged into EIS - SIT
    Creates a new Customer with a Relationship and creates a Motor Policy

    Background: Create an Individual Customer without Relationship
        Given I Login into the EIS SIT
        And I create a Customer without Relationship
        Then I validate the Customer Details "without" Relationship

    # Scenario: Create an Individual Customer with Individual as Relationship
    #     Given I Login into the EIS SIT
    #     And I create a Customer with Individual Relationship
    #     Then I validate the Customer Details "with" Relationship
    # Background: Customer Search
    #     Given I Login into the EIS SIT
    #     And I search for "Customer" with string "Santhosh Ippili Tester"

    Scenario Outline: Create a New Motor Policy for a Customer
        Given I create the "<PolicyType>" Policy for the Customer
        When I enter a "<PolicyType>" , "<PolicyStyle>" Policy information and Policy Count "<PolicyCount>"
        And I enter the "<PolicyType>" Insured information
        And I enter the "<PolicyStyle>" Details
        And I enter the Driver information for "<PolicyStyle>"
        And I enter the FormsAddl info
        And I choose the plan "<Plan>" Premium&Coverages for "<PolicyStyle>"
        And I Finish Quote in Document Request Tab

        And I create a new billing account with "<BillingPeriod>"
        ####values include:Monthly,Weekly
        And I Create a Payment Method of "<PaymentType>"
        ####Payment method values: CreditCard,PCI,DirectDebit
        And I enable "<PaymentOptions>" Payment Options
        ####payment options include: Policy,Billing,both
        And I choose the Paymentplan to "<PaymentPlan>"
        ####payment plan eligible values: SixMonthly,Quarterly,Monthly,Annual
        Then I Purchase the policy by clicking on finish
        Examples:
            | PolicyType | PolicyStyle | PolicyCount | Plan | BillingPeriod | PaymentType | PaymentOptions | PaymentPlan |
# | Motor      | Personal Motor   | 1           | Comprehensive Agreed | Monthly       | CreditCard  | both           | Annual      |
# | Motor      | Commercial Motor | 1           | Comprehensive Market | Monthly       | DirectDebit | both           | Monthly     |


# Scenario: Create a New Motorbike Policy for a Customer

#     Given I create the "Motor" Policy for the Customer
#     When I enter a "Motor" , "Motorbike" Policy information and Policy Count "1"
#     And I enter the "Motor" Insured information
#     And I enter the "Motorbike" Details
#     And I enter the Driver information for "Motorbike"
#     And I enter the FormsAddl info
#     And I choose the plan "TPFT" Premium&Coverages for "Motorbike"

# Scenario: Create a New Caravan Policy for a Customer

#     Given I create the "Motor" Policy for the Customer
#     When I enter a "Motor" , "Caravan" Policy information and Policy Count "1"
#     And I enter the "Motor" Insured information
#     And I enter the "Caravan" Details
#     And I enter the Driver information for "Caravan"
#     And I enter the FormsAddl info
#     And I choose the plan "Comprehensive Agreed" Premium&Coverages for "Caravan"


# Scenario: Create a New Motorhome Policy for a Customer
#     Given I create the "Motor" Policy for the Customer
#     When I enter a "Motor" , "Motorhome" Policy information and Policy Count "1"
#     And I enter the "Motor" Insured information
#     And I enter the "Motorhome" Details
#     And I enter the Driver information for "Motorhome"
#     And I enter the FormsAddl info
#     And I enter the Premium&Coverages for "Motorhome"


# Scenario: Create a New Trailer Policy for a Customer
#     Given I create the "Motor" Policy for the Customer
#     When I enter a "Motor" , "Trailer (stand alone)" Policy information and Policy Count "1"
#     And I enter the "Motor" Insured information
#     And I enter the "Trailer (stand alone)" Details
#     And I enter the Driver information for "Trailer (stand alone)"
#     And I enter the FormsAddl info
#     And I enter the Premium&Coverages for "Trailer (stand alone)"

