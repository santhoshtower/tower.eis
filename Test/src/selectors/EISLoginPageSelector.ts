
const eisLoginPageSelector = {

    giveLoginName: './/*[@id="loginForm:j_username"]',
    givePwd: '//*[@id="loginForm:j_password"]',
    clickLogin: '//*[@id="loginForm:submitForm"]',
}

export default eisLoginPageSelector;