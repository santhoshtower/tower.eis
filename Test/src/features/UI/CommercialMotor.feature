@ui
Feature: All Commercial Motor Scenarios Including billing
    Background: Create an Individual Customer without Relationship
        Given I Login into the EIS SIT
        And I create a Customer without Relationship
        Then I validate the Customer Details "without" Relationship

    Scenario Outline: Create a New Motor Policy for a Commercial Motor
        Given I create the "<PolicyType>" Policy for the Customer
        When I enter a "<PolicyType>" , "<PolicyStyle>" Policy information and Policy Count "<PolicyCount>"
        And I enter the "<PolicyType>" Insured information
        And I enter the "<PolicyStyle>" Details
        And I enter the Driver information for "<PolicyStyle>"
        And I enter the FormsAddl info
        And I choose the plan "<Plan>" Premium&Coverages for "<PolicyStyle>"
        And I Finish Quote in Document Request Tab
        And I create a new billing account with "<BillingPeriod>"
        ####values include:Monthly,Weekly
        And I Create a Payment Method of "<PaymentType>"
        ####Payment method values: CreditCard,PCI,DirectDebit
        And I enable "<PaymentOptions>" Payment Options
        ####payment options include: Policy,Billing,both
        And I choose the Paymentplan to "<PaymentPlan>"
        ####payment plan eligible values: SixMonthly,Quarterly,Monthly,Annual
        Then I Purchase the policy by clicking on finish
        Examples:
            | PolicyType | PolicyStyle      | PolicyCount | Plan                 | BillingPeriod | PaymentType | PaymentOptions | PaymentPlan |
            | Motor      | Commercial Motor | 1           | Comprehensive Market | Monthly       | CreditCard  | both           | Annual      |
            | Motor      | Commercial Motor | 1           | Comprehensive Market | Monthly       | DirectDebit | both           | Annual      |
            | Motor      | Commercial Motor | 1           | Comprehensive Market | Weekly        | CreditCard  | both           |             |
            | Motor      | Commercial Motor | 1           | Comprehensive Market | Weekly        | DirectDebit | both           |             |
            | Motor      | Commercial Motor | 1           | Comprehensive Market | Monthly       | CreditCard  | both           | SixMonthly  |
            | Motor      | Commercial Motor | 1           | Comprehensive Market | Monthly       | DirectDebit | both           | SixMonthly  |
            #| Motor      | Commercial Motor | 1           | Comprehensive Market | Weekly        | DirectDebit | both           | SixMonthly  |
            #| Motor      | Commercial Motor | 1           | Comprehensive Market | Weekly        | CreditCard  | both           | SixMonthly  |
            | Motor | Commercial Motor | 1 | Comprehensive Market | Monthly | DirectDebit | both | Quarterly |
            | Motor | Commercial Motor | 1 | Comprehensive Market | Monthly | CreditCard  | both | Quarterly |
            #| Motor      | Commercial Motor | 1           | Comprehensive Market | Weekly        | DirectDebit | both           | Quarterly   |
            #| Motor      | Commercial Motor | 1           | Comprehensive Market | Weekly        | CreditCard  | both           | Quarterly   |
            | Motor | Commercial Motor | 1 | Comprehensive Market | Monthly | DirectDebit | both | Monthly |
            | Motor | Commercial Motor | 1 | Comprehensive Market | Monthly | CreditCard  | both | Monthly |
#| Motor      | Commercial Motor | 1           | Comprehensive Market | Weekly        | DirectDebit | both           | Monthly     |
#| Motor      | Commercial Motor | 1           | Comprehensive Market | Weekly        | CreditCard  | both           | Monthly     |
