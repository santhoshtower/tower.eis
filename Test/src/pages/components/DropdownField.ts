import { IComponent } from "./BaseComponent";

class DropdownField implements IComponent {
    private textField;
    setValue(selectors: any, value: string | number) {

        browser.waitForExist(selectors.elementSelector);
        this.textField = browser.$(selectors.elementSelector);
        this.textField.setValue(value);

        // waiting for result to show
        if (selectors.resultSelector) {
            browser.waitForExist(selectors.resultSelector);
            browser.click(selectors.resultSelector);
            browser.waitForExist(selectors.resultSelector, 2000, true);
        }
    }
}

export { DropdownField }
