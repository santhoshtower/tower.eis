import { expect } from 'chai';
import { Given, When, Then } from 'cucumber';
import EISLoginPage from '../../pages/EISLoginPage';
import EISHomePage from '../../pages/EISHomePage';
import eisQuotePage from '../../pages/EISQuotePage';
import MotorPage from '../../pages/EISMotorPage';
import { CustomerData } from '../../common/CustomerData';
import { SharedData } from '../../common/CommonData';
//import { EISLogin } from '../../steps/UI/EISLogin';

var environment = CustomerData.Environment.Tac;
const page = new EISLoginPage(environment);
const homePage = new EISHomePage(environment);
const quotePage = new eisQuotePage(environment);
const motorPage = new MotorPage(environment);

var address = CustomerData.ValidInd.SearchAddress;

//TODO: use the parameters firstname and lastname
When(/I Create a Customer with Airpoints (.*), (.*), (.*)$/, async (firstname, lastname, airpointsNumber: string) => {
    homePage.createCustomerWithoutRelationship(CustomerData.ValidInd.Salutation, SharedData.firstname, SharedData.lastname, CustomerData.ValidInd.Gender, CustomerData.ValidInd.DOB, address, airpointsNumber);
    // homePage.clickCustomerTab.click();
    // homePage.clickCreateCustomer.click();
    // var btnStatus = homePage.btnYes.isEnabled();
    // if (homePage.btnYes.isEnabled() == false) {
    //     expect(btnStatus).to.be.false;
    // }
    // console.log(firstName + " " + lastName);
    // homePage.rdBtnCreateIndiviudal.click();
    // homePage.btnYes.click();
    // homePage.dpdwnSalutation.selectByVisibleText(CustomerData.ValidInd.Salutation);
    // homePage.txtbxFirstName.setValue(firstName);
    // homePage.txtbxLastName.setValue(lastName);
    // homePage.dpdwnGender.selectByVisibleText(CustomerData.ValidInd.Gender);
    // homePage.txtBoxDOB.setValue(CustomerData.ValidInd.DOB);

    //Airpoints come into action



    //BAU
    // homePage.txtbxSearchAddress.setValue(address);
    // browser.waitForSpinner();
    // browser.pause(1000);
    // homePage.selectAddress.click();
    // browser.pause(1000);
    // browser.waitForSpinner();
    // homePage.rdBtnPaperless.scroll(0, 1500);
    // browser.waitForSpinner();
    // browser.pause(1000);
    // browser.click('//*[@id="crmForm:additionalInfo_paperless"]');
    // browser.waitForSpinner();
    // homePage.selectNext();
    // homePage.selectDone();
});

When(/^I run for (.*) many times$/, async () => {
});
