import { expect } from 'chai';
import { Given, When, Then } from 'cucumber';
import EISLoginPage from '../../pages/EISLoginPage';
import EISHomePage from '../../pages/EISHomePage';
import { CustomerData } from '../../common/CustomerData';
import EISRelationshipPage from '../../pages/EISRelationshipPage';
import { SharedData } from '../../common/CommonData';
import { Person } from '../../common/Person';

var rltnFirstName = "Relationship First Name" + SharedData.uniqueId2;
var rltnLastName = "Relationship Last Name" + SharedData.uniqueId2;

var environment = CustomerData.Environment.Tac;
const loginPage = new EISLoginPage(environment);
const homePage = new EISHomePage(environment);
const rltnPage = new EISRelationshipPage(environment);

var address = CustomerData.ValidInd.SearchAddress;
var addressForScript = CustomerData.ValidInd.SearchAddressValidation;

Given('I Login into the EIS SIT', (): void => {
    loginPage.open();
    browser.windowHandleFullscreen();
    loginPage.login("qa", "qa");
});

Given('I create a Customer without Relationship', async function () {
    homePage.createCustomerWithoutRelationship(CustomerData.ValidInd.Salutation, SharedData.firstname, SharedData.lastname, CustomerData.ValidInd.Gender, CustomerData.ValidInd.DOB, address, null);
});

Given(/^I Create a Non-Indi Customer of type "([^"]*)?" with relationship and relationship "([^"]*)?"$/, async (CustomerType, RelationshipType) => {
    if (CustomerType == "Non-Indi") {
        homePage.createCustomerWithRelationship(address, SharedData.uniqueId1);
        rltnPage.selectAddRelationship();
        this.waitForSpinner();
    }
});

Given(/^I Create a Non-Indi Customer of type "([^"]*)?" WithOut relationship$/, async (CustomerType, NonIndiType) => {
    homePage.createCustomerWithRelationship(address, SharedData.uniqueId1);
});

Given('I create a Customer with Individual Relationship', (): void => {
    var addRelationshipPage = homePage.createCustomerWithIndividualRelationship(CustomerData.ValidInd, rltnFirstName, rltnLastName, address);
    var relationshipIndividual = new Person(CustomerData.RelationshipInd.Salutation, rltnFirstName, rltnLastName, CustomerData.RelationshipInd.Gender, CustomerData.RelationshipInd.DOB);
    addRelationshipPage.addRelationship(relationshipIndividual, CustomerData.RelationshipInd.Relationship);
});

Then(/^I validate the Customer Details "([^"]*)?" Relationship$/, async (CustomerRelationship) => {
    if (CustomerRelationship == "without") {
        expect(homePage.getCustomerName()).to.be.equal(SharedData.firstname + " " + SharedData.lastname);
        expect(homePage.getAddress()).to.be.equal(addressForScript);
    } else if (CustomerRelationship == "with") {
        console.log("in else if");
        console.log(homePage.getCustomerName());
        console.log(homePage.getAddress());

        expect(homePage.getCustomerName()).to.be.equal(SharedData.firstname + " " + SharedData.lastname);
        expect(homePage.getAddress()).to.be.equal(addressForScript);

        rltnPage.selectTab();

        expect(rltnPage.getCustomerNameLabel()).to.be.equal("Customer " + SharedData.firstname + " " + SharedData.lastname + " Contact Details");
        expect(rltnPage.getRelationshipNameLabel()).to.be.equal("Individual " + rltnFirstName + " " + rltnLastName + " " + "( " + CustomerData.RelationshipInd.Relationship + " )");
    }
});



