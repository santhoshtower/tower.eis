''' To add new customer detail to be created when seeding customer data'''
# Create a new file to conatin the customer info
1. create a copy of TemplateCustomer.ts in this folder
2. Name your file apprioriately, just needs to be unique e.g. YourNewCustomerName.ts
    Inside the file change 
        'const TemplateCustomer = {' 
    to 
        'const YourNewCustomerName = {' 
    I suggest you use the customer name you set above
3. At the bottom of file update the export change the line
        export { TemplateCustomer };
    To use the customer name you set above
        export { YourNewCustomerName };

# This template contains all available fields.
4. Fill in required fields with values you want
5. Optional - Remove any unwantd fields


# hook up your new set of customer data to the code that adds it via the api as per below
6. In SeedTestDataSteps.ts Add an import for your new file
    import { YourNewCustomerName } from '../../data/CustomerDataCreation/Customers/YourNewCustomerName';
7. Add a line of code inside the step hook for 'I create customers based on json data'
    CustomerDataService.createCustomer(YourNewCustomerName);

# Done, you new customer data will be added along with the others when the Scenario 'Create a set of customers from stored file' is next run