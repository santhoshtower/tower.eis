const debug = process.env.WDIO_DEBUG;
const debugSepc = process.env.SPEC;
const maxInstances = process.env.MAX_INSTANCES || 5;

module.exports = {
    debug: debug,
    execArgv: debug ? ['--inspect=127.0.0.1:9229'] : [],
    logLevel: 'error',
    maxInstances: maxInstances,
    capabilities:
        [
            {
                browserName: 'chrome',
                chromeOptions: process.env.UI_IN_HEADLESS ? {
                    args: ['--headless', '--disable-gpu', '--window-size=1280,800'],
                } : null,
                specs: debug ?
                    [
                        debugSepc
                    ] :
                    [
                        // './src/features/UI/EISHouseNewCustomer.feature',
                        // './src/features/UI/EISCreateCustomer.feature',
                        './src/features/UI/EISMotorNewCustomer.feature',
                        // './src/features/UI/EISSearchPolicy.feature',
                        // './src/features/UI/*.feature'
                        // './src/features/UI/PersonalMotor.feature',
                        // './src/features/UI/Test.feature',
                        // './src/features/UI/House.feature',
                        // './src/features/UI/EISCustomer.feature',
                        // './src/features/UI/Trailer.feature',
                    ],
            }
        ],
    cucumberOpts: {
        tagExpression: '(@ui) and not @Pending and not @prod',
    }
};