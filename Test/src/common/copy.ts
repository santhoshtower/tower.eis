
const copy = {

    TradePersonMsg: "Just so you know... If you keep your tools in this vehicle, they won’t be covered by this insurance policy. Instead, you’ll need to take out a business assets policy. We’ll give you details of how to do this at the end of this process.",
    OtherMsg: "The details you've given mean that we're currently unable to offer you cover. If you would like to know more, fire up a live chat or give us a call on 0800 XXX XXX, and we can discuss your options",
    mainDriverMsg: "We need to ask a few questions about the main driver of the vehicle. This is the person that will drive the vehicle most of the time. We need these details so that we can assess the level of risk. We'll grab the names of the main drivers later on.",
    businessUseToolTip: "We’re asking this because it’s important that you’re covered by the right insurance policy. If your car is used for business and you only have insurance for a personal vehicle, you won’t be covered if you need to make a claim.",
};

export default copy;