export class SharedData {
    static fullname: any;
    static legalName: any;
    static lastCC4N0: any;
    static firstname: any;
    static lastname: any;
    static uniqueId1: any;
    static uniqueId2: any;
    static rltnFirstName: any;
    static rltnLastName: any;
    static legalname: any;

    static reset() {
        this.fullname = null;
        this.lastCC4N0 = null;
        this.legalName = null;
        this.firstname = null;
        this.lastname = null;
        this.uniqueId1 = null;
        this.uniqueId2 = null;
        this.rltnFirstName = null;
        this.rltnLastName = null;
        this.legalname = null;
        this.fullname = null;
    }

    static generateUniqueId() {

        SharedData.uniqueId1 = Math.random().toString(36).substring(2) + (new Date()).getTime().toString(36);
        SharedData.uniqueId2 = Math.random().toString(36).substring(3) + (new Date()).getTime().toString(36);

        SharedData.firstname = "TR Billing" + SharedData.uniqueId1;
        SharedData.lastname = "TRB Lname" + SharedData.uniqueId1;

        SharedData.rltnFirstName = "Relationship First Name" + SharedData.uniqueId2;
        SharedData.rltnLastName = "Relationship Last Name" + SharedData.uniqueId2;

        SharedData.fullname = SharedData.firstname + " " + SharedData.lastname;
        SharedData.legalName = "NameLegal" + " " + SharedData.uniqueId1;
    }

}