import BasePage from './BasePage';
import { CustomerData } from '../common/CustomerData';
import { SharedData } from '../common/SharedData';

export default class BillingPage extends BasePage {
    get chkBoxCreateNewAcc(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="purchaseForm:billingAccount_createNewAccount"]') };
    get dpdwnDuedayType(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="purchaseForm:accountDueDayComponent_dueDayType"]') };
    get dpdwnBillingState(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="purchaseForm:billingAccount_billingAccountDetails_billingAccountAddress_stateProvCd"]') };
    get btnBillingValidate(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="purchaseForm:billingAccount_billingAccountDetails_billingAccountAddress_addressValidate:validateButton"]') };
    get rdbtnResolvedaddress(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="purchaseForm:billingAccount_billingAccountDetails_billingAccountAddress_addressValidate:resolvedAddressResults_data"]/tr/td[1]/div/div[2]') };
    get btnBillingPopupOk(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="purchaseForm:billingAccount_billingAccountDetails_billingAccountAddress_addressValidate:addressValidationPopupOkBtn"]') };
    get btnAddPaymentMthd(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="purchaseForm:addPaymentMethodBtn"]') };
    get chkBoxEnableRecurringPayment_Policy(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="purchaseForm:billablePolicyTermRecurringPayments_billingAutomaticRecurring"]') };
    get dpdwnUsePaymentMthd_Policy(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="purchaseForm:billablePolicyTermRecurringPayments_paymentType"]') };
    get chkBoxEnableRecurringPayment_Billing(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="purchaseForm:billingAcccountRecurringPayments_billingAutomaticRecurring"]') };
    get dpdwnUsePaymentMthd_Billing(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="purchaseForm:billingAcccountRecurringPayments_paymentType"]') };
    get dpdwnPaymentPlan(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="purchaseForm:paymentPlanSelectMenu"]') };

    // payment method page
    get dpdwnPaymentMethod(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="paymentMethodTypeFormSwitch:paymentMethodType"]') };
    get dpdwnPaymentMthdState(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="paymentMethodForm:addressPaymentMethod_billableAddres_stateProvCd"]') };
    get btnPaymentMthdValidate(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="paymentMethodForm:addressValidate:validateButton"]') };
    get rdbtnPyamentMthdResolvedAddress(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="paymentMethodForm:addressValidate:resolvedAddressResults_data"]/tr/td[1]/div/div[2]') };
    get btnPaymentMthdPopUpOK(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="paymentMethodForm:addressValidate:addressValidationPopupOkBtn"]') };

    //Credit Card information
    get dpdwnCardType(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="paymentMethodForm:generalPaymentMethod_displayType"]') };
    get txtBoxCCHName(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="paymentMethodForm:generalPaymentMethod_fullName"]') };
    get txtBoxCCNo(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="paymentMethodForm:generalPaymentMethod_fullNumber"]') };
    get txtBoxVN(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="paymentMethodForm:generalPaymentMethod_securityCode"]') };
    get dpdwnCCExpMonth(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="paymentMethodForm:expMonth"]') };
    get dpdwnCCExpYear(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="paymentMethodForm:expYear"]') };
    get btnAddorUpdate(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="paymentMethodForm:saveBtn"]') };
    get btnAddorUpdate_EFT(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="paymentMethodEFTForm:saveBtn"]') };
    get btnBackFooter(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="primaryButtonsForm:backButton_footer"]') };

    //Direct Debit Information.
    get txtBoxBankID(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="paymentMethodEFTForm:paymentEFT_bankID"]') };
    get txtBoxBranch(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="paymentMethodEFTForm:paymentEFT_bankBranch"]') };
    get txtBoxBaseAccNo(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="paymentMethodEFTForm:paymentEFT_accountBaseNumber"]') };
    get txtBoxAccSuffix(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="paymentMethodEFTForm:paymentEFT_accountSuffix"]') };
    get txtBoxBankName(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="paymentMethodEFTForm:paymentEFT_bankName"]') };
    get btnBankValidate(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="paymentMethodEFTForm:moduloValidate"]') };
    get dpdwnpaymentMethodToBeUsedFor(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="paymentMethodEFTForm:paymentMethodToBeUsedFor"]') };
    get rdbtnAuthRx(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="paymentMethodEFTForm:authorisationReceived:0"]') };
    get rdbtnFormSign(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="paymentMethodEFTForm:setupDirectDebitAuthorityWithoutSigningForm:0"]') };
    get rdbtn9months(): WebdriverIO.Client<WebdriverIO.RawResult<WebdriverIO.Element>> { return browser.element('//*[@id="paymentMethodEFTForm:cancelledDirectDebitAuthorityWithTowerInsurance:0"]') };

    public selectPaymentPlan(Paymentplan) {
        this.dpdwnPaymentPlan.selectByValue(Paymentplan);
        browser.waitForSpinner();
    }

    public enablePolicyOption() {
        this.chkBoxEnableRecurringPayment_Policy.scroll(0, 500);
        browser.clickAndWait(this.chkBoxEnableRecurringPayment_Policy);
        this.dpdwnUsePaymentMthd_Policy.selectByIndex(1);
        browser.waitForSpinner();
    }

    public enableBillingOption() {
        this.chkBoxEnableRecurringPayment_Policy.scroll(0, 500); //?????
        this.chkBoxEnableRecurringPayment_Billing.click();
        browser.waitForSpinner();
        this.dpdwnUsePaymentMthd_Billing.selectByIndex(1);
        browser.waitForSpinner();
    }

    public createNewAccount(DueDayType) {
        this.chkBoxCreateNewAcc.click();
        browser.waitForSpinner();
        if (DueDayType == "Monthly") {
            this.dpdwnDuedayType.selectByIndex(0);
        } else if (DueDayType == "Weekly") {
            this.dpdwnDuedayType.selectByIndex(1);
        }
        browser.waitForSpinner();
    }

    public createPaymentMethod(PaymentType) {
        this.btnAddPaymentMthd.scroll(0, 500);
        this.btnAddPaymentMthd.click();
        browser.waitForSpinner();

        if (PaymentType == "CreditCard") {
            this.createCreditCardPayment();
        }
        else if (PaymentType == "PCI") {
            this.createPCIPayment();

        } else if (PaymentType == "DirectDebit") {
            this.createDebitPayment();
        } else {
            console.log("Please use proper value at PaymentType: Monthly, PCI, DirectDebit")
        }

        this.btnBackFooter.click();
        browser.waitForSpinner();
    }

    public createDebitPayment() {
        this.dpdwnPaymentMethod.selectByIndex(2);
        browser.waitForSpinner();
        this.txtBoxBankID.setValue(CustomerData.BiilingDetails.BankID);
        browser.waitForSpinner();
        browser.keys("Tab");
        browser.waitForSpinner();
        this.txtBoxBranch.setValue(CustomerData.BiilingDetails.Branch);
        browser.waitForSpinner();
        browser.keys("Tab");
        browser.waitForSpinner();
        this.txtBoxBaseAccNo.setValue(CustomerData.BiilingDetails.BaseAccNo);
        browser.waitForSpinner();
        browser.keys("Tab");
        browser.waitForSpinner();
        this.txtBoxAccSuffix.setValue(CustomerData.BiilingDetails.Suffix);
        browser.waitForSpinner();
        browser.keys("Tab");
        browser.waitForSpinner();
        this.btnBankValidate.click();
        browser.waitForSpinner();
        browser.keys("Tab");
        browser.waitForSpinner();
        this.dpdwnpaymentMethodToBeUsedFor.selectByIndex(1);
        browser.waitForSpinner();
        this.rdbtnAuthRx.click();
        browser.waitForSpinner();
        this.rdbtnFormSign.click();
        browser.waitForSpinner();
        this.rdbtn9months.scroll(0, 500);
        this.rdbtn9months.click();
        browser.waitForSpinner();
        //dd account = 03 0104 0188327 020

        this.btnAddorUpdate_EFT.scroll(0, 500);
        this.btnAddorUpdate_EFT.click();
        browser.waitForSpinner();
    }

    public createPCIPayment() {
        this.dpdwnPaymentMethod.selectByIndex(0);
        browser.waitForSpinner();
        this.dpdwnPaymentMthdState.selectByIndex(1);
        // browser.waitForSpinner();
        // this.btnPaymentMthdValidate.click();
        // browser.waitForSpinner();
        // this.rdbtnPyamentMthdResolvedAddress.click();
        // browser.waitForSpinner();
        // this.btnPaymentMthdPopUpOK.click();
        browser.waitForSpinner();

        this.dpdwnPaymentMethod.selectByIndex(1);
        browser.waitForSpinner();
        this.txtBoxCCNo.setValue(CustomerData.BiilingDetails.CCNo);
        browser.waitForSpinner();
        browser.keys("Tab");
        browser.waitForSpinner();
        this.txtBoxCCHName.setValue(CustomerData.BiilingDetails.CCName);
        browser.waitForSpinner();
        browser.keys("Tab");
        browser.waitForSpinner();
        this.btnAddorUpdate.scroll(0, 500);
        this.btnAddorUpdate.click();
        browser.waitForSpinner();
    }

    public createCreditCardPayment() {
        this.dpdwnPaymentMethod.selectByIndex(0);
        browser.waitForSpinner();
        this.dpdwnPaymentMthdState.selectByIndex(1);
        browser.waitForSpinner();
        // this.btnPaymentMthdValidate.click();
        // browser.waitForSpinner();
        // this.rdbtnPyamentMthdResolvedAddress.click();
        // browser.waitForSpinner();
        // this.btnPaymentMthdPopUpOK.click();
        // browser.waitForSpinner();
        var CCNo = CustomerData.BiilingDetails.CCNo;
        var last4CCNo = CCNo.substr(CCNo.length - 4);

        SharedData.lastCC4N0 = last4CCNo;
        console.log(SharedData.lastCC4N0);
        this.dpdwnCardType.selectByIndex(1);
        browser.waitForSpinner();
        this.txtBoxCCNo.setValue(CustomerData.BiilingDetails.CCNo);
        browser.waitForSpinner();
        browser.keys("Tab");
        browser.waitForSpinner();
        this.txtBoxCCHName.setValue(CustomerData.BiilingDetails.CCName);
        browser.waitForSpinner();
        browser.keys("Tab");
        browser.waitForSpinner();
        this.txtBoxVN.setValue(CustomerData.BiilingDetails.CCCVV);
        browser.waitForSpinner();
        browser.keys("Tab");
        browser.waitForSpinner();
        this.btnAddorUpdate.scroll(0, 500);
        this.btnAddorUpdate.click();
        browser.waitForSpinner();
    }
}
