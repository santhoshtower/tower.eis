import { expect } from 'chai';
import { Given, When, Then } from 'cucumber';
import EISLoginPage from '../../pages/EISLoginPage';
import EISHomePage from '../../pages/EISHomePage';
import eisQuotePage from '../../pages/EISQuotePage';
import MotorPage from '../../pages/EISMotorPage';
import BillingPage from '../../pages/BillingPage';
import { CustomerData } from '../../common/CustomerData';
import { SharedData } from '../../common/CommonData';
//import { EISLogin } from '../../steps/UI/EISLogin';

var environment = CustomerData.Environment.Tac;
const page = new EISLoginPage(environment);
const homePage = new EISHomePage(environment);
const quotePage = new eisQuotePage(environment);
const motorPage = new MotorPage(environment);
const billingPage = new BillingPage(environment);

When(/^I create a new billing account with "([^"]*)?"$/, async (DueDayType) => {
    billingPage.createNewAccount(DueDayType);

});
When(/^I Create a Payment Method of "([^"]*)?"$/, async (PaymentType) => {
    billingPage.createPaymentMethod(PaymentType);
});
When(/^I enable "([^"]*)?" Payment Options/, async (PaymentOption) => {
    if (PaymentOption == "Policy") {
        billingPage.enablePolicyOption();
        // xpath for dynamic test data //*[@id="updateForm:billingAcccountRecurringPayments_paymentType"]/option[contains(text(), '2121')]
    } else if (PaymentOption == "Billing") {
        billingPage.enableBillingOption();
    } else if (PaymentOption == "both") {
        billingPage.enablePolicyOption();
        billingPage.enableBillingOption();
    }
});

When(/^I choose the Paymentplan to "([^"]*)?"$/, async (Paymentplan) => {
    if (Paymentplan !== undefined && Paymentplan !== "") {
        console.log("Paymentplan", Paymentplan);
        billingPage.selectPaymentPlan(Paymentplan);
    }
});

Then(/^I Purchase the policy by clicking on finish/, (): void => {
    billingPage.dpdwnBillingState.selectByIndex(1);
    browser.waitForSpinner();
    motorPage.selectFinish();
    browser.waitForSpinner();
    //  console.log(motorPage.lblPolicyNo.getText());
    SharedData.reset();
});